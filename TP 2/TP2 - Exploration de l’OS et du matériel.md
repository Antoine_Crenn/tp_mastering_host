# TP2 - Exploration de l'OS et du matériel

## Sujet 4 : Analyse de boot

### Exercices

#### Exo 1

On cherche à déterminer les 5 processus les plus long à démarrer au boot de l'OS pour cela on va faire la commande systemd-analyse plot > graphe.svg qui permet de créer un fichier graphe.svg ou il y a un graphe qui réprensente les différets processus qui sont lancés au démarrage de l'OS.

![](https://i.imgur.com/0fOVvwQ.png)


Le résultat de la commande nous montre que les 5 processus les plus long à démarrer au boot de l'OS sont systemd puis systemd-journald.service puis dm-event.socket, system-selinux\x2dpolicy\x2dmigrate\x2dlocal\x2dchanges.slice et enfin lvm2-lvmetad.socket

#### Exo 2

On cherche la liste des périphériques vus par le noyau et dans quel ordre pour ce faire on fait la commande sudo dmesg | grep -i input pour afficher que les périphériques.

```
[antoine@localhost ~]# sudo dmesg | grep -i input
[    0.647739] input: Power Button as /devices/LNXSYSTM:00/LNXPWRBN:00/input/input0
[    0.647853] input: Sleep Button as /devices/LNXSYSTM:00/LNXSLPBN:00/input/input1
[    0.718654] input: AT Translated Set 2 keyboard as /devices/platform/i8042/serio0/input/input2
[    0.920694] input: ImExPS/2 Generic Explorer Mouse as /devices/platform/i8042/serio1/input/input3
[    3.684598] input: Video Bus as /devices/LNXSYSTM:00/device:00/PNP0A03:00/LNXVIDEO:00/input/input4
[    3.768475] input: PC Speaker as /devices/platform/pcspkr/input/input5
```

Voila les périphériques qui sont vus par le noyau le plus en haut est le premier vu et ainsi de suite.

Maintenant on cherche la version du noyau pour ce faire on fait la commande sudo dmesg | grep -i linux

```
[antoine@localhost ~]# sudo dmesg | grep -i linux
[    0.000000] Linux version 3.10.0-1127.8.2.el7.x86_64 (mockbuild@kbuilder.bsys.centos.org) (gcc version 4.8.5 20150623 (Red Hat 4.8.5-39) (GCC) ) #1 SMP Tue May 12 16:57:42 UTC 2020
[    0.050372] SELinux:  Initializing.
[    0.050380] SELinux:  Starting in permissive mode
[    0.248551] EVM: security.selinux
[    0.251096] ACPI: Added _OSI(Linux-Dell-Video)
[    0.646286] SELinux:  Registering netfilter hooks
[    0.648602] Linux agpgart interface v0.103
[    0.660452] usb usb1: Manufacturer: Linux 3.10.0-1127.8.2.el7.x86_64 ehci_hcd
[    0.717137] usb usb2: Manufacturer: Linux 3.10.0-1127.8.2.el7.x86_64 ohci_hcd
[    0.720584] Loaded X.509 cert 'CentOS Linux kpatch signing key: ea0413152cde1d98ebdca3fe6f0230904c9ef717'
[    0.720595] Loaded X.509 cert 'CentOS Linux Driver update signing key: 7f421ee0ab69461574bb358861dbe77762a4201b'
[    0.720913] Loaded X.509 cert 'CentOS Linux kernel signing key: ff318ce9b8324e455770c26ee8bf608139ffa928'
[    0.731380] systemd[1]: systemd 219 running in system mode. (+PAM +AUDIT +SELINUX +IMA -APPARMOR +SMACK +SYSVINIT +UTMP +LIBCRYPTSETUP +GCRYPT +GNUTLS +ACL +XZ +LZ4 -SECCOMP +BLKID +ELFUTILS +KMOD +IDN)
[    3.096247] SELinux: 2048 avtab hash slots, 112685 rules.
[    3.142921] SELinux: 2048 avtab hash slots, 112685 rules.
[    3.170875] SELinux:  8 users, 14 roles, 5046 types, 316 bools, 1 sens, 1024 cats
[    3.170878] SELinux:  130 classes, 112685 rules
[    3.175925] SELinux:  Completing initialization.
[    3.175928] SELinux:  Setting up existing superblocks.
[    3.187657] systemd[1]: Successfully loaded SELinux policy in 106.622ms.
```

La version du noyau utilisé est Linux version 3.10.0-1127.8.2.el7.x86_64

On cherche 5 lignes de la chaine de boot qui semblent importantes pour ce faire on fait la commande sudo dmesg.

```
[antoine@localhost ~]# sudo dmesg
[    0.000000] Initializing cgroup subsys cpuset
[    0.000000] Initializing cgroup subsys cpu
[    0.000000] Initializing cgroup subsys cpuacct
[    0.000000] Linux version 3.10.0-1127.8.2.el7.x86_64 (mockbuild@kbuilder.bsys.centos.org) (gcc version 4.8.5 20150623 (Red Hat 4.8.5-39) (GCC) ) #1 SMP Tue May 12 16:57:42 UTC 2020
[    0.000000] Command line: BOOT_IMAGE=/vmlinuz-3.10.0-1127.8.2.el7.x86_64 root=/dev/mapper/centos-root ro crashkernel=auto spectre_v2=retpoline rd.lvm.lv=centos/root rd.lvm.lv=centos/swap rhgb quiet LANG=en_US.UTF-8
[    0.000000] e820: BIOS-provided physical RAM map:
[    0.000000] BIOS-e820: [mem 0x0000000000000000-0x000000000009fbff] usable
[    0.000000] BIOS-e820: [mem 0x000000000009fc00-0x000000000009ffff] reserved
[    0.000000] BIOS-e820: [mem 0x00000000000f0000-0x00000000000fffff] reserved
[    0.000000] BIOS-e820: [mem 0x0000000000100000-0x000000003ffeffff] usable
[    0.000000] BIOS-e820: [mem 0x000000003fff0000-0x000000003fffffff] ACPI data
[    0.000000] BIOS-e820: [mem 0x00000000fec00000-0x00000000fec00fff] reserved
[    0.000000] BIOS-e820: [mem 0x00000000fee00000-0x00000000fee00fff] reserved
[    0.000000] BIOS-e820: [mem 0x00000000fffc0000-0x00000000ffffffff] reserved
[    0.000000] NX (Execute Disable) protection: active
[    0.000000] SMBIOS 2.5 present.
[    0.000000] DMI: innotek GmbH VirtualBox/VirtualBox, BIOS VirtualBox 12/01/2006
[    0.000000] Hypervisor detected: KVM
[    0.000000] e820: update [mem 0x00000000-0x00000fff] usable ==> reserved
[    0.000000] e820: remove [mem 0x000a0000-0x000fffff] usable
[    0.000000] e820: last_pfn = 0x3fff0 max_arch_pfn = 0x400000000
[    0.000000] MTRR default type: uncachable
[    0.000000] MTRR variable ranges disabled:
[    0.000000] PAT configuration [0-7]: WB  WC  UC- UC  WB  WP  UC- UC
[    0.000000] CPU MTRRs all blank - virtualized system.
[    0.000000] found SMP MP-table at [mem 0x0009fff0-0x0009ffff] mapped at [ffffffffff200ff0]
[    0.000000] Base memory trampoline at [ffff976240099000] 99000 size 24576
[    0.000000] BRK [0x21473000, 0x21473fff] PGTABLE
[    0.000000] BRK [0x21474000, 0x21474fff] PGTABLE
[    0.000000] BRK [0x21475000, 0x21475fff] PGTABLE
[    0.000000] BRK [0x21476000, 0x21476fff] PGTABLE
[    0.000000] BRK [0x21477000, 0x21477fff] PGTABLE
[    0.000000] RAMDISK: [mem 0x35797000-0x36bc3fff]
[    0.000000] Early table checksum verification disabled
[    0.000000] ACPI: RSDP 00000000000e0000 00024 (v02 VBOX  )
[    0.000000] ACPI: XSDT 000000003fff0030 0003C (v01 VBOX   VBOXXSDT 00000001 ASL  00000061)
[    0.000000] ACPI: FACP 000000003fff00f0 000F4 (v04 VBOX   VBOXFACP 00000001 ASL  00000061)
[    0.000000] ACPI: DSDT 000000003fff0470 022EA (v02 VBOX   VBOXBIOS 00000002 INTL 20100528)
[    0.000000] ACPI: FACS 000000003fff0200 00040
[    0.000000] ACPI: APIC 000000003fff0240 00054 (v02 VBOX   VBOXAPIC 00000001 ASL  00000061)
[    0.000000] ACPI: SSDT 000000003fff02a0 001CC (v01 VBOX   VBOXCPUT 00000002 INTL 20100528)
[    0.000000] ACPI: Local APIC address 0xfee00000
[    0.000000] No NUMA configuration found
[    0.000000] Faking a node at [mem 0x0000000000000000-0x000000003ffeffff]
[    0.000000] NODE_DATA(0) allocated [mem 0x3ffc9000-0x3ffeffff]
[    0.000000] crashkernel=auto resulted in zero bytes of reserved memory.
[    0.000000] kvm-clock: cpu 0, msr 0:3ff78001, primary cpu clock
[    0.000000] kvm-clock: Using msrs 4b564d01 and 4b564d00
[    0.000000] kvm-clock: using sched offset of 9281792588 cycles
[    0.000000] Zone ranges:
[    0.000000]   DMA      [mem 0x00001000-0x00ffffff]
[    0.000000]   DMA32    [mem 0x01000000-0xffffffff]
[    0.000000]   Normal   empty
[    0.000000] Movable zone start for each node
[    0.000000] Early memory node ranges
[    0.000000]   node   0: [mem 0x00001000-0x0009efff]
[    0.000000]   node   0: [mem 0x00100000-0x3ffeffff]
[    0.000000] Initmem setup node 0 [mem 0x00001000-0x3ffeffff]
[    0.000000] On node 0 totalpages: 262030
[    0.000000]   DMA zone: 64 pages used for memmap
[    0.000000]   DMA zone: 21 pages reserved
[    0.000000]   DMA zone: 3998 pages, LIFO batch:0
[    0.000000]   DMA32 zone: 4032 pages used for memmap
[    0.000000]   DMA32 zone: 258032 pages, LIFO batch:31
[    0.000000] ACPI: PM-Timer IO Port: 0x4008
[    0.000000] ACPI: Local APIC address 0xfee00000
[    0.000000] ACPI: LAPIC (acpi_id[0x00] lapic_id[0x00] enabled)
[    0.000000] ACPI: IOAPIC (id[0x01] address[0xfec00000] gsi_base[0])
[    0.000000] IOAPIC[0]: apic_id 1, version 32, address 0xfec00000, GSI 0-23
[    0.000000] ACPI: INT_SRC_OVR (bus 0 bus_irq 0 global_irq 2 dfl dfl)
[    0.000000] ACPI: INT_SRC_OVR (bus 0 bus_irq 9 global_irq 9 low level)
[    0.000000] ACPI: IRQ0 used by override.
[    0.000000] ACPI: IRQ9 used by override.
[    0.000000] Using ACPI (MADT) for SMP configuration information
[    0.000000] smpboot: Allowing 1 CPUs, 0 hotplug CPUs
[    0.000000] PM: Registered nosave memory: [mem 0x0009f000-0x0009ffff]
[    0.000000] PM: Registered nosave memory: [mem 0x000a0000-0x000effff]
[    0.000000] PM: Registered nosave memory: [mem 0x000f0000-0x000fffff]
[    0.000000] e820: [mem 0x40000000-0xfebfffff] available for PCI devices
[    0.000000] Booting paravirtualized kernel on KVM
[    0.000000] setup_percpu: NR_CPUS:5120 nr_cpumask_bits:1 nr_cpu_ids:1 nr_node_ids:1
[    0.000000] percpu: Embedded 38 pages/cpu s118784 r8192 d28672 u2097152
[    0.000000] pcpu-alloc: s118784 r8192 d28672 u2097152 alloc=1*2097152
[    0.000000] pcpu-alloc: [0] 0
[    0.000000] Built 1 zonelists in Node order, mobility grouping on.  Total pages: 257913
[    0.000000] Policy zone: DMA32
[    0.000000] Kernel command line: BOOT_IMAGE=/vmlinuz-3.10.0-1127.8.2.el7.x86_64 root=/dev/mapper/centos-root ro crashkernel=auto spectre_v2=retpoline rd.lvm.lv=centos/root rd.lvm.lv=centos/swap rhgb quiet LANG=en_US.UTF-8
[    0.000000] PID hash table entries: 4096 (order: 3, 32768 bytes)
[    0.000000] x86/fpu: xstate_offset[2]: 0240, xstate_sizes[2]: 0100
[    0.000000] xsave: enabled xstate_bv 0x7, cntxt size 0x340 using standard form
[    0.000000] Memory: 991160k/1048512k available (7784k kernel code, 392k absent, 56960k reserved, 5957k data, 1980k init)
[    0.000000] SLUB: HWalign=64, Order=0-3, MinObjects=0, CPUs=1, Nodes=1
[    0.000000] Hierarchical RCU implementation.
[    0.000000]  RCU restricting CPUs from NR_CPUS=5120 to nr_cpu_ids=1.
[    0.000000] NR_IRQS:327936 nr_irqs:256 0
[    0.000000] Console: colour VGA+ 80x25
[    0.000000] console [tty0] enabled
[    0.000000] allocated 4194304 bytes of page_cgroup
[    0.000000] please try 'cgroup_disable=memory' option if you don't want memory cgroups
[    0.000000] tsc: Detected 1992.005 MHz processor
[    0.085608] Calibrating delay loop (skipped) preset value.. 3984.01 BogoMIPS (lpj=1992005)
[    0.085612] pid_max: default: 32768 minimum: 301
[    0.085649] Security Framework initialized
[    0.085656] SELinux:  Initializing.
[    0.085669] SELinux:  Starting in permissive mode
[    0.085670] Yama: becoming mindful.
[    0.090226] Dentry cache hash table entries: 131072 (order: 8, 1048576 bytes)
[    0.090468] Inode-cache hash table entries: 65536 (order: 7, 524288 bytes)
[    0.090618] Mount-cache hash table entries: 2048 (order: 2, 16384 bytes)
[    0.090624] Mountpoint-cache hash table entries: 2048 (order: 2, 16384 bytes)
[    0.090900] Initializing cgroup subsys memory
[    0.090908] Initializing cgroup subsys devices
[    0.090911] Initializing cgroup subsys freezer
[    0.090914] Initializing cgroup subsys net_cls
[    0.090917] Initializing cgroup subsys blkio
[    0.090919] Initializing cgroup subsys perf_event
[    0.090923] Initializing cgroup subsys hugetlb
[    0.090925] Initializing cgroup subsys pids
[    0.090928] Initializing cgroup subsys net_prio
[    0.092690] Last level iTLB entries: 4KB 0, 2MB 0, 4MB 0
[    0.092694] Last level dTLB entries: 4KB 64, 2MB 0, 4MB 0
[    0.092696] tlb_flushall_shift: 6
[    0.092706] FEATURE SPEC_CTRL Not Present
[    0.092708] FEATURE IBPB_SUPPORT Not Present
[    0.092710] Spectre V1 : Mitigation: Load fences, usercopy/swapgs barriers and __user pointer sanitization
[    0.092716] Spectre V2 : retpoline selected on command line.
[    0.093503] Spectre V2 : Vulnerable: Retpoline without IBPB
[    0.093507] Speculative Store Bypass: Vulnerable
[    0.093652] MDS: Mitigation: Clear CPU buffers
[    0.128518] Freeing SMP alternatives: 28k freed
[    0.147359] ACPI: Core revision 20130517
[    0.149589] ACPI: All ACPI Tables successfully acquired
[    0.149902] ftrace: allocating 29617 entries in 116 pages
[    0.250068] Enabling x2apic
[    0.250081] Enabled x2apic
[    0.250480] Switched APIC routing to physical x2apic.
[    0.252711] ..TIMER: vector=0x30 apic1=0 pin1=2 apic2=-1 pin2=-1
[    0.252724] smpboot: CPU0: Intel(R) Core(TM) i7-8565U CPU @ 1.80GHz (fam: 06, model: 8e, stepping: 0b)
[    0.349467] Performance Events: unsupported p6 CPU model 142 no PMU driver, software events only.
[    0.349504] KVM setup paravirtual spinlock
[    0.355918] Brought up 1 CPUs
[    0.355924] smpboot: Max logical packages: 1
[    0.355927] smpboot: Total of 1 processors activated (3984.01 BogoMIPS)
[    0.356646] devtmpfs: initialized
[    0.356758] x86/mm: Memory block size: 128MB
[    0.359359] EVM: security.selinux
[    0.359364] EVM: security.ima
[    0.359366] EVM: security.capability
[    0.361627] atomic64 test passed for x86-64 platform with CX8 and with SSE
[    0.361633] pinctrl core: initialized pinctrl subsystem
[    0.361710] RTC time: 13:25:42, date: 06/04/20
[    0.361820] NET: Registered protocol family 16
[    0.362038] cpuidle: using governor haltpoll
[    0.362133] ACPI: bus type PCI registered
[    0.362135] acpiphp: ACPI Hot Plug PCI Controller Driver version: 0.5
[    0.362281] PCI: Using configuration type 1 for base access
[    0.363992] ACPI: Added _OSI(Module Device)
[    0.363995] ACPI: Added _OSI(Processor Device)
[    0.363997] ACPI: Added _OSI(3.0 _SCP Extensions)
[    0.363999] ACPI: Added _OSI(Processor Aggregator Device)
[    0.364001] ACPI: Added _OSI(Linux-Dell-Video)
[    0.364548] ACPI: EC: Look up EC in DSDT
[    0.364912] ACPI: Executed 1 blocks of module-level executable AML code
[    0.368042] ACPI: Interpreter enabled
[    0.368054] ACPI: (supports S0 S5)
[    0.368057] ACPI: Using IOAPIC for interrupt routing
[    0.368279] PCI: Using host bridge windows from ACPI; if necessary, use "pci=nocrs" and report a bug
[    0.368412] ACPI: Enabled 2 GPEs in block 00 to 07
[    0.374423] ACPI: PCI Root Bridge [PCI0] (domain 0000 [bus 00-ff])
[    0.374434] acpi PNP0A03:00: _OSC: OS supports [ASPM ClockPM Segments MSI]
[    0.375117] acpi PNP0A03:00: _OSC: not requesting OS control; OS requires [ExtendedConfig ASPM ClockPM MSI]
[    0.375144] acpi PNP0A03:00: fail to add MMCONFIG information, can't access extended PCI configuration space under this bridge.
[    0.375699] PCI host bridge to bus 0000:00
[    0.375706] pci_bus 0000:00: root bus resource [io  0x0000-0x0cf7 window]
[    0.375709] pci_bus 0000:00: root bus resource [io  0x0d00-0xffff window]
[    0.375713] pci_bus 0000:00: root bus resource [mem 0x000a0000-0x000bffff window]
[    0.375717] pci_bus 0000:00: root bus resource [mem 0x40000000-0xfdffffff window]
[    0.375720] pci_bus 0000:00: root bus resource [bus 00-ff]
[    0.375799] pci 0000:00:00.0: [8086:1237] type 00 class 0x060000
[    0.376873] pci 0000:00:01.0: [8086:7000] type 00 class 0x060100
[    0.378263] pci 0000:00:01.1: [8086:7111] type 00 class 0x01018a
[    0.379070] pci 0000:00:01.1: reg 0x20: [io  0xd000-0xd00f]
[    0.379480] pci 0000:00:01.1: legacy IDE quirk: reg 0x10: [io  0x01f0-0x01f7]
[    0.379484] pci 0000:00:01.1: legacy IDE quirk: reg 0x14: [io  0x03f6]
[    0.379487] pci 0000:00:01.1: legacy IDE quirk: reg 0x18: [io  0x0170-0x0177]
[    0.379490] pci 0000:00:01.1: legacy IDE quirk: reg 0x1c: [io  0x0376]
[    0.379949] pci 0000:00:02.0: [15ad:0405] type 00 class 0x030000
[    0.385306] pci 0000:00:02.0: reg 0x10: [io  0xd010-0xd01f]
[    0.390901] pci 0000:00:02.0: reg 0x14: [mem 0xf0000000-0xf7ffffff]
[    0.396955] pci 0000:00:02.0: reg 0x18: [mem 0xf8000000-0xf81fffff]
[    0.415464] pci 0000:00:03.0: [8086:100e] type 00 class 0x020000
[    0.416621] pci 0000:00:03.0: reg 0x10: [mem 0xf8200000-0xf821ffff]
[    0.418109] pci 0000:00:03.0: reg 0x18: [io  0xd020-0xd027]
[    0.421846] pci 0000:00:04.0: [80ee:cafe] type 00 class 0x088000
[    0.422873] pci 0000:00:04.0: reg 0x10: [io  0xd040-0xd05f]
[    0.424327] pci 0000:00:04.0: reg 0x14: [mem 0xf8400000-0xf87fffff]
[    0.425786] pci 0000:00:04.0: reg 0x18: [mem 0xf8800000-0xf8803fff pref]
[    0.430215] pci 0000:00:05.0: [8086:2415] type 00 class 0x040100
[    0.430397] pci 0000:00:05.0: reg 0x10: [io  0xd100-0xd1ff]
[    0.430555] pci 0000:00:05.0: reg 0x14: [io  0xd200-0xd23f]
[    0.431645] pci 0000:00:06.0: [106b:003f] type 00 class 0x0c0310
[    0.432708] pci 0000:00:06.0: reg 0x10: [mem 0xf8804000-0xf8804fff]
[    0.445442] pci 0000:00:07.0: [8086:7113] type 00 class 0x068000
[    0.447003] pci 0000:00:07.0: quirk: [io  0x4000-0x403f] claimed by PIIX4 ACPI
[    0.447024] pci 0000:00:07.0: quirk: [io  0x4100-0x410f] claimed by PIIX4 SMB
[    0.448295] pci 0000:00:08.0: [8086:100e] type 00 class 0x020000
[    0.449595] pci 0000:00:08.0: reg 0x10: [mem 0xf8820000-0xf883ffff]
[    0.452071] pci 0000:00:08.0: reg 0x18: [io  0xd240-0xd247]
[    0.456123] pci 0000:00:0b.0: [8086:265c] type 00 class 0x0c0320
[    0.457328] pci 0000:00:0b.0: reg 0x10: [mem 0xf8840000-0xf8840fff]
[    0.462507] pci 0000:00:0d.0: [8086:2829] type 00 class 0x010601
[    0.463251] pci 0000:00:0d.0: reg 0x10: [io  0xd248-0xd24f]
[    0.463847] pci 0000:00:0d.0: reg 0x14: [io  0xd250-0xd253]
[    0.464498] pci 0000:00:0d.0: reg 0x18: [io  0xd258-0xd25f]
[    0.465070] pci 0000:00:0d.0: reg 0x1c: [io  0xd260-0xd263]
[    0.465552] pci 0000:00:0d.0: reg 0x20: [io  0xd270-0xd27f]
[    0.466303] pci 0000:00:0d.0: reg 0x24: [mem 0xf8842000-0xf8843fff]
[    0.469473] ACPI: PCI Interrupt Link [LNKA] (IRQs 5 9 10 *11)
[    0.469787] ACPI: PCI Interrupt Link [LNKB] (IRQs 5 9 *10 11)
[    0.469921] ACPI: PCI Interrupt Link [LNKC] (IRQs 5 *9 10 11)
[    0.470039] ACPI: PCI Interrupt Link [LNKD] (IRQs 5 9 10 *11)
[    0.470427] vgaarb: device added: PCI:0000:00:02.0,decodes=io+mem,owns=io+mem,locks=none
[    0.470431] vgaarb: loaded
[    0.470432] vgaarb: bridge control possible 0000:00:02.0
[    0.470555] SCSI subsystem initialized
[    0.470606] ACPI: bus type USB registered
[    0.470644] usbcore: registered new interface driver usbfs
[    0.470657] usbcore: registered new interface driver hub
[    0.470677] usbcore: registered new device driver usb
[    0.470820] EDAC MC: Ver: 3.0.0
[    0.471414] PCI: Using ACPI for IRQ routing
[    0.471417] PCI: pci_cache_line_size set to 64 bytes
[    0.471732] e820: reserve RAM buffer [mem 0x0009fc00-0x0009ffff]
[    0.471739] e820: reserve RAM buffer [mem 0x3fff0000-0x3fffffff]
[    0.471900] NetLabel: Initializing
[    0.471902] NetLabel:  domain hash size = 128
[    0.471903] NetLabel:  protocols = UNLABELED CIPSOv4
[    0.471925] NetLabel:  unlabeled traffic allowed by default
[    0.472028] amd_nb: Cannot enumerate AMD northbridges
[    0.472045] Switched to clocksource kvm-clock
[    0.493761] pnp: PnP ACPI init
[    0.493779] ACPI: bus type PNP registered
[    0.493875] pnp 00:00: Plug and Play ACPI device, IDs PNP0303 (active)
[    0.493994] pnp 00:01: Plug and Play ACPI device, IDs PNP0f03 (active)
[    0.494884] pnp: PnP ACPI: found 2 devices
[    0.494886] ACPI: bus type PNP unregistered
[    0.501141] pci_bus 0000:00: resource 4 [io  0x0000-0x0cf7 window]
[    0.501146] pci_bus 0000:00: resource 5 [io  0x0d00-0xffff window]
[    0.501149] pci_bus 0000:00: resource 6 [mem 0x000a0000-0x000bffff window]
[    0.501151] pci_bus 0000:00: resource 7 [mem 0x40000000-0xfdffffff window]
[    0.501186] NET: Registered protocol family 2
[    0.501430] TCP established hash table entries: 8192 (order: 4, 65536 bytes)
[    0.501450] TCP bind hash table entries: 8192 (order: 5, 131072 bytes)
[    0.501462] TCP: Hash tables configured (established 8192 bind 8192)
[    0.501480] TCP: reno registered
[    0.501484] UDP hash table entries: 512 (order: 2, 16384 bytes)
[    0.501489] UDP-Lite hash table entries: 512 (order: 2, 16384 bytes)
[    0.501522] NET: Registered protocol family 1
[    0.501542] pci 0000:00:00.0: Limiting direct PCI/PCI transfers
[    0.501582] pci 0000:00:01.0: Activating ISA DMA hang workarounds
[    0.501625] pci 0000:00:02.0: Boot video device
[    0.504312] PCI: CLS 0 bytes, default 64
[    0.504359] Unpacking initramfs...
[    0.937130] Freeing initrd memory: 20660k freed
[    0.940513] RAPL PMU: API unit is 2^-32 Joules, 5 fixed counters, 10737418240 ms ovfl timer
[    0.940518] RAPL PMU: hw unit of domain pp0-core 2^-0 Joules
[    0.940520] RAPL PMU: hw unit of domain package 2^-0 Joules
[    0.940521] RAPL PMU: hw unit of domain dram 2^-0 Joules
[    0.940523] RAPL PMU: hw unit of domain pp1-gpu 2^-0 Joules
[    0.940524] RAPL PMU: hw unit of domain psys 2^-0 Joules
[    0.940748] platform rtc_cmos: registered platform RTC device (no PNP device found)
[    0.941184] sha1_ssse3: Using AVX optimized SHA-1 implementation
[    0.941265] sha256_ssse3: Using AVX optimized SHA-256 implementation
[    0.941593] futex hash table entries: 256 (order: 2, 16384 bytes)
[    0.941614] Initialise system trusted keyring
[    0.941645] audit: initializing netlink socket (disabled)
[    0.941659] type=2000 audit(1591277154.001:1): initialized
[    0.973865] HugeTLB registered 2 MB page size, pre-allocated 0 pages
[    0.975513] zpool: loaded
[    0.975517] zbud: loaded
[    0.975889] VFS: Disk quotas dquot_6.5.2
[    0.975922] Dquot-cache hash table entries: 512 (order 0, 4096 bytes)
[    0.976179] Key type big_key registered
[    0.976181] SELinux:  Registering netfilter hooks
[    0.976744] NET: Registered protocol family 38
[    0.976750] Key type asymmetric registered
[    0.976753] Asymmetric key parser 'x509' registered
[    0.976797] Block layer SCSI generic (bsg) driver version 0.4 loaded (major 248)
[    0.976825] io scheduler noop registered
[    0.976828] io scheduler deadline registered (default)
[    0.976856] io scheduler cfq registered
[    0.976859] io scheduler mq-deadline registered
[    0.976862] io scheduler kyber registered
[    0.976990] pci_hotplug: PCI Hot Plug PCI Core version: 0.5
[    0.976998] pciehp: PCI Express Hot Plug Controller Driver version: 0.4
[    0.977011] shpchp: Standard Hot Plug PCI Controller Driver version: 0.4
[    0.977311] ACPI: AC Adapter [AC] (on-line)
[    0.977371] input: Power Button as /devices/LNXSYSTM:00/LNXPWRBN:00/input/input0
[    0.977374] ACPI: Power Button [PWRF]
[    0.977470] input: Sleep Button as /devices/LNXSYSTM:00/LNXSLPBN:00/input/input1
[    0.977473] ACPI: Sleep Button [SLPF]
[    0.977620] GHES: HEST is not enabled!
[    0.977695] Serial: 8250/16550 driver, 4 ports, IRQ sharing enabled
[    0.978056] Non-volatile memory driver v1.3
[    0.978125] Linux agpgart interface v0.103
[    0.978279] crash memory driver: version 1.1
[    0.978337] rdac: device handler registered
[    0.978361] hp_sw: device handler registered
[    0.978365] emc: device handler registered
[    0.983206] ACPI: Battery Slot [BAT0] (battery present)
[    0.983241] alua: device handler registered
[    0.983360] libphy: Fixed MDIO Bus: probed
[    0.983414] ehci_hcd: USB 2.0 'Enhanced' Host Controller (EHCI) Driver
[    0.983420] ehci-pci: EHCI PCI platform driver
[    0.984165] ehci-pci 0000:00:0b.0: EHCI Host Controller
[    0.984216] ehci-pci 0000:00:0b.0: new USB bus registered, assigned bus number 1
[    0.984488] ehci-pci 0000:00:0b.0: irq 19, io mem 0xf8840000
[    0.990118] ehci-pci 0000:00:0b.0: USB 2.0 started, EHCI 1.00
[    0.990210] usb usb1: New USB device found, idVendor=1d6b, idProduct=0002, bcdDevice= 3.10
[    0.990213] usb usb1: New USB device strings: Mfr=3, Product=2, SerialNumber=1
[    0.990216] usb usb1: Product: EHCI Host Controller
[    0.990218] usb usb1: Manufacturer: Linux 3.10.0-1127.8.2.el7.x86_64 ehci_hcd
[    0.990220] usb usb1: SerialNumber: 0000:00:0b.0
[    0.990350] hub 1-0:1.0: USB hub found
[    0.990356] hub 1-0:1.0: 12 ports detected
[    0.990675] ohci_hcd: USB 1.1 'Open' Host Controller (OHCI) Driver
[    0.990685] ohci-pci: OHCI PCI platform driver
[    0.991667] ohci-pci 0000:00:06.0: OHCI PCI host controller
[    0.991743] ohci-pci 0000:00:06.0: new USB bus registered, assigned bus number 2
[    0.991851] ohci-pci 0000:00:06.0: irq 22, io mem 0xf8804000
[    1.043404] usb usb2: New USB device found, idVendor=1d6b, idProduct=0001, bcdDevice= 3.10
[    1.043408] usb usb2: New USB device strings: Mfr=3, Product=2, SerialNumber=1
[    1.043411] usb usb2: Product: OHCI PCI host controller
[    1.043414] usb usb2: Manufacturer: Linux 3.10.0-1127.8.2.el7.x86_64 ohci_hcd
[    1.043416] usb usb2: SerialNumber: 0000:00:06.0
[    1.043568] hub 2-0:1.0: USB hub found
[    1.043586] hub 2-0:1.0: 12 ports detected
[    1.044043] uhci_hcd: USB Universal Host Controller Interface driver
[    1.044113] usbcore: registered new interface driver usbserial_generic
[    1.044121] usbserial: USB Serial support registered for generic
[    1.044172] i8042: PNP: PS/2 Controller [PNP0303:PS2K,PNP0f03:PS2M] at 0x60,0x64 irq 1,12
[    1.044786] serio: i8042 KBD port at 0x60,0x64 irq 1
[    1.044792] serio: i8042 AUX port at 0x60,0x64 irq 12
[    1.044891] mousedev: PS/2 mouse device common for all mice
[    1.045215] input: AT Translated Set 2 keyboard as /devices/platform/i8042/serio0/input/input2
[    1.046099] random: fast init done
[    1.046594] rtc_cmos rtc_cmos: rtc core: registered rtc_cmos as rtc0
[    1.046677] rtc_cmos rtc_cmos: alarms up to one day, 114 bytes nvram
[    1.046829] hidraw: raw HID events driver (C) Jiri Kosina
[    1.046957] usbcore: registered new interface driver usbhid
[    1.046958] usbhid: USB HID core driver
[    1.047022] drop_monitor: Initializing network drop monitor service
[    1.047087] TCP: cubic registered
[    1.047091] Initializing XFRM netlink socket
[    1.047249] NET: Registered protocol family 10
[    1.047426] NET: Registered protocol family 17
[    1.047432] mpls_gso: MPLS GSO support
[    1.047600] mce: Using 0 MCE banks
[    1.051486] PM: Hibernation image not present or could not be loaded.
[    1.051491] Loading compiled-in X.509 certificates
[    1.051514] Loaded X.509 cert 'CentOS Linux kpatch signing key: ea0413152cde1d98ebdca3fe6f0230904c9ef717'
[    1.051528] Loaded X.509 cert 'CentOS Linux Driver update signing key: 7f421ee0ab69461574bb358861dbe77762a4201b'
[    1.051960] Loaded X.509 cert 'CentOS Linux kernel signing key: ff318ce9b8324e455770c26ee8bf608139ffa928'
[    1.051986] registered taskstats version 1
[    1.054345] Key type trusted registered
[    1.056672] Key type encrypted registered
[    1.056695] IMA: No TPM chip found, activating TPM-bypass! (rc=-19)
[    1.056827]   Magic number: 8:53:432
[    1.056949] rtc_cmos rtc_cmos: setting system clock to 2020-06-04 13:25:43 UTC (1591277143)
[    1.057802] Freeing unused kernel memory: 1980k freed
[    1.058074] Write protecting the kernel read-only data: 12288k
[    1.059396] Freeing unused kernel memory: 396k freed
[    1.061483] Freeing unused kernel memory: 540k freed
[    1.067241] systemd[1]: systemd 219 running in system mode. (+PAM +AUDIT +SELINUX +IMA -APPARMOR +SMACK +SYSVINIT +UTMP +LIBCRYPTSETUP +GCRYPT +GNUTLS +ACL +XZ +LZ4 -SECCOMP +BLKID +ELFUTILS +KMOD +IDN)
[    1.067267] systemd[1]: Detected virtualization kvm.
[    1.067276] systemd[1]: Detected architecture x86-64.
[    1.067280] systemd[1]: Running in initial RAM disk.
[    1.067316] systemd[1]: Set hostname to <localhost.localdomain>.
[    1.132358] systemd[1]: Created slice Root Slice.
[    1.132586] systemd[1]: Created slice System Slice.
[    1.132613] systemd[1]: Reached target Slices.
[    1.132632] systemd[1]: Reached target Timers.
[    1.132695] systemd[1]: Listening on udev Kernel Socket.
[    1.132794] systemd[1]: Listening on Journal Socket.
[    1.133558] systemd[1]: Starting Setup Virtual Console...
[    1.133658] systemd[1]: Listening on udev Control Socket.
[    1.133699] systemd[1]: Reached target Local File Systems.
[    1.133725] systemd[1]: Reached target Swap.
[    1.134659] systemd[1]: Starting Apply Kernel Variables...
[    1.135499] systemd[1]: Starting dracut cmdline hook...
[    1.136057] systemd[1]: Starting Journal Service...
[    1.136701] systemd[1]: Starting Create list of required static device nodes for the current kernel...
[    1.136732] systemd[1]: Reached target Sockets.
[    1.146850] systemd[1]: Started Create list of required static device nodes for the current kernel.
[    1.150288] systemd[1]: Started Apply Kernel Variables.
[    1.151063] systemd[1]: Starting Create Static Device Nodes in /dev...
[    1.243047] systemd[1]: Started Create Static Device Nodes in /dev.
[    1.246081] psmouse serio1: alps: Unknown ALPS touchpad: E7=10 00 64, EC=10 00 64
[    1.247487] input: ImExPS/2 Generic Explorer Mouse as /devices/platform/i8042/serio1/input/input3
[    1.288666] systemd[1]: Started Journal Service.
[    1.568346] device-mapper: uevent: version 1.0.3
[    1.568448] device-mapper: ioctl: 4.37.1-ioctl (2018-04-03) initialised: dm-devel@redhat.com
[    1.859626] e1000: Intel(R) PRO/1000 Network Driver - version 7.3.21-k8-NAPI
[    1.859629] e1000: Copyright (c) 1999-2006 Intel Corporation.
[    2.001491] tsc: Refined TSC clocksource calibration: 1989.015 MHz
[    2.004399] libata version 3.00 loaded.
[    2.591777] e1000 0000:00:03.0 eth0: (PCI:33MHz:32-bit) 08:00:27:2a:93:3b
[    2.591782] e1000 0000:00:03.0 eth0: Intel(R) PRO/1000 Network Connection
[    2.591806] ata_piix 0000:00:01.1: version 2.13
[    2.592410] scsi host0: ata_piix
[    2.592979] scsi host1: ata_piix
[    2.593025] ata1: PATA max UDMA/33 cmd 0x1f0 ctl 0x3f6 bmdma 0xd000 irq 14
[    2.593027] ata2: PATA max UDMA/33 cmd 0x170 ctl 0x376 bmdma 0xd008 irq 15
[    2.593145] ahci 0000:00:0d.0: version 3.0
[    2.593829] ahci 0000:00:0d.0: SSS flag set, parallel bus scan disabled
[    2.593999] ahci 0000:00:0d.0: AHCI 0001.0100 32 slots 1 ports 3 Gbps 0x1 impl SATA mode
[    2.594004] ahci 0000:00:0d.0: flags: 64bit ncq stag only ccc
[    2.595646] scsi host2: ahci
[    2.595705] ata3: SATA max UDMA/133 abar m8192@0xf8842000 port 0xf8842100 irq 21
[    2.596903] [drm] DMA map mode: Keeping DMA mappings.
[    2.596951] [drm] Capabilities:
[    2.596952] [drm]   Cursor.
[    2.596953] [drm]   Cursor bypass 2.
[    2.596954] [drm]   Alpha cursor.
[    2.596955] [drm]   3D.
[    2.596956] [drm]   Extended Fifo.
[    2.596957] [drm]   Pitchlock.
[    2.596958] [drm]   Irq mask.
[    2.596959] [drm]   GMR.
[    2.596960] [drm]   Traces.
[    2.596961] [drm]   GMR2.
[    2.596962] [drm]   Screen Object 2.
[    2.596964] [drm] Max GMR ids is 8192
[    2.596965] [drm] Max number of GMR pages is 1048576
[    2.596967] [drm] Max dedicated hypervisor surface memory is 393216 kiB
[    2.596968] [drm] Maximum display memory size is 131072 kiB
[    2.596969] [drm] VRAM at 0xf0000000 size is 131072 kiB
[    2.596971] [drm] MMIO at 0xf8000000 size is 2048 kiB
[    2.597520] [TTM] Zone  kernel: Available graphics memory: 507382 kiB
[    2.597522] [TTM] Initializing pool allocator
[    2.597527] [TTM] Initializing DMA pool allocator
[    2.597548] [drm] Supports vblank timestamp caching Rev 2 (21.10.2013).
[    2.597549] [drm] No driver support for vblank timestamp query.
[    2.597825] [drm] Screen Objects Display Unit initialized
[    2.597896] [drm] width 720
[    2.597909] [drm] height 400
[    2.597922] [drm] bpp 32
[    2.598415] [drm] Fifo max 0x00200000 min 0x00001000 cap 0x00000355
[    2.598424] [drm] DX: no.
[    2.598426] [drm] Atomic: yes.
[    2.598427] [drm] SM4_1: no.
[    2.598443] [drm:vmw_host_log [vmwgfx]] *ERROR* Failed to send host log message.
[    2.600055] [drm:vmw_host_log [vmwgfx]] *ERROR* Failed to send host log message.
[    2.615168] fbcon: svgadrmfb (fb0) is primary device
[    2.644275] Console: switching to colour frame buffer device 100x37
[    2.645172] [drm] Initialized vmwgfx 2.15.0 20180704 for 0000:00:02.0 on minor 0
[    2.746557] ata2.00: ATAPI: VBOX CD-ROM, 1.0, max UDMA/133
[    2.747005] ata2.00: configured for UDMA/33
[    2.747565] scsi 1:0:0:0: CD-ROM            VBOX     CD-ROM           1.0  PQ: 0 ANSI: 5
[    2.905377] ata3: SATA link up 3.0 Gbps (SStatus 123 SControl 300)
[    2.905587] ata3.00: ATA-6: VBOX HARDDISK, 1.0, max UDMA/133
[    2.905590] ata3.00: 16777216 sectors, multi 128: LBA48 NCQ (depth 31/32)
[    2.911187] ata3.00: configured for UDMA/133
[    2.911308] scsi 2:0:0:0: Direct-Access     ATA      VBOX HARDDISK    1.0  PQ: 0 ANSI: 5
[    2.951003] sd 2:0:0:0: [sda] 16777216 512-byte logical blocks: (8.58 GB/8.00 GiB)
[    2.951103] sd 2:0:0:0: [sda] Write Protect is off
[    2.951107] sd 2:0:0:0: [sda] Mode Sense: 00 3a 00 00
[    2.955160] sd 2:0:0:0: [sda] Write cache: enabled, read cache: enabled, doesn't support DPO or FUA
[    2.956306]  sda: sda1 sda2
[    2.956752] sd 2:0:0:0: [sda] Attached SCSI disk
[    3.207809] e1000 0000:00:08.0 eth1: (PCI:33MHz:32-bit) 08:00:27:c4:aa:62
[    3.207825] e1000 0000:00:08.0 eth1: Intel(R) PRO/1000 Network Connection
[    3.256841] sr 1:0:0:0: [sr0] scsi3-mmc drive: 32x/32x xa/form2 tray
[    3.256847] cdrom: Uniform CD-ROM driver Revision: 3.20
[    3.257742] sr 1:0:0:0: Attached scsi CD-ROM sr0
[    3.676592] SGI XFS with ACLs, security attributes, no debug enabled
[    3.680728] XFS (dm-0): Mounting V5 Filesystem
[    3.936582] random: crng init done
[    3.953297] XFS (dm-0): Starting recovery (logdev: internal)
[    3.965812] XFS (dm-0): Ending recovery (logdev: internal)
[    4.367058] systemd-journald[92]: Received SIGTERM from PID 1 (systemd).
[    4.434101] type=1404 audit(1591277146.876:2): enforcing=1 old_enforcing=0 auid=4294967295 ses=4294967295
[    4.454365] SELinux: 2048 avtab hash slots, 112685 rules.
[    4.550986] SELinux: 2048 avtab hash slots, 112685 rules.
[    4.609910] SELinux:  8 users, 14 roles, 5046 types, 316 bools, 1 sens, 1024 cats
[    4.609915] SELinux:  130 classes, 112685 rules
[    4.618439] SELinux:  Completing initialization.
[    4.618444] SELinux:  Setting up existing superblocks.
[    4.624891] type=1403 audit(1591277147.067:3): policy loaded auid=4294967295 ses=4294967295
[    4.632789] systemd[1]: Successfully loaded SELinux policy in 198.995ms.
[    4.671564] ip_tables: (C) 2000-2006 Netfilter Core Team
[    4.671637] systemd[1]: Inserted module 'ip_tables'
[    4.712674] systemd[1]: Relabelled /dev, /run and /sys/fs/cgroup in 35.881ms.
[    5.119901] systemd-journald[479]: Received request to flush runtime journal from PID 1
[    5.396712] ACPI: Video Device [GFX0] (multi-head: yes  rom: no  post: no)
[    5.396814] input: Video Bus as /devices/LNXSYSTM:00/device:00/PNP0A03:00/LNXVIDEO:00/input/input4
[    5.486930] piix4_smbus 0000:00:07.0: SMBus Host Controller at 0x4100, revision 0
[    5.521396] input: PC Speaker as /devices/platform/pcspkr/input/input5
[    5.556169] cryptd: max_cpu_qlen set to 1000
[    5.581090] sr 1:0:0:0: Attached scsi generic sg0 type 5
[    5.581161] sd 2:0:0:0: Attached scsi generic sg1 type 0
[    5.693597] AVX2 version of gcm_enc/dec engaged.
[    5.693600] AES CTR mode by8 optimization enabled
[    5.871784] ppdev: user-space parallel port driver
[    5.912934] alg: No test for __gcm-aes-aesni (__driver-gcm-aes-aesni)
[    5.920598] alg: No test for __generic-gcm-aes-aesni (__driver-generic-gcm-aes-aesni)
[    5.926496] Adding 839676k swap on /dev/mapper/centos-swap.  Priority:-2 extents:1 across:839676k FS
[    5.973246] XFS (sda1): Mounting V5 Filesystem
[    6.111893] intel_pmc_core:  initialized
[    6.352862] XFS (sda1): Ending clean mount
[    8.148307] snd_intel8x0 0000:00:05.0: white list rate for 1028:0177 is 48000
[    8.174027] type=1305 audit(1591277150.616:4): audit_pid=625 old=0 auid=4294967295 ses=4294967295 subj=system_u:system_r:auditd_t:s0 res=1
[    8.608471] floppy0: no floppy controllers found
[    8.608523] work still pending
[   10.061926] ip6_tables: (C) 2000-2006 Netfilter Core Team
[   10.242742] Ebtables v2.0 registered
[   10.292993] Netfilter messages via NETLINK v0.30.
[   10.316532] ip_set: protocol 7
[   10.380130] IPv6: ADDRCONF(NETDEV_UP): enp0s3: link is not ready
[   10.386123] IPv6: ADDRCONF(NETDEV_UP): enp0s3: link is not ready
[   10.386563] e1000: enp0s3 NIC Link is Up 1000 Mbps Full Duplex, Flow Control: RX
[   10.389223] IPv6: ADDRCONF(NETDEV_CHANGE): enp0s3: link becomes ready
[   10.448964] IPv6: ADDRCONF(NETDEV_UP): enp0s8: link is not ready
[   10.454877] IPv6: ADDRCONF(NETDEV_UP): enp0s8: link is not ready
[   10.455523] e1000: enp0s8 NIC Link is Up 1000 Mbps Full Duplex, Flow Control: RX
[   10.455928] IPv6: ADDRCONF(NETDEV_CHANGE): enp0s8: link becomes ready
[   11.102069] nf_conntrack version 0.5.0 (7927 buckets, 31708 max)
[   11.324402] bridge: filtering via arp/ip/ip6tables is no longer available by default. Update your scripts to load br_netfilter if you need this.
```

La première ligne choisit est [    0.000000] BIOS-e820: [mem 0x000000000009fc00-0x000000000009ffff] reserved cette ligne montre que le bios réserve un emplacement mémoire. 
La deuxième ligne chosit est [    0.090908] Initializing cgroup subsys devices cette ligne montre qu'il y a l'initalisation du groupe qui rassemble les elements de la machine. 
La troisième ligne choisit est [    1.051960] Loaded X.509 cert 'CentOS Linux kernel signing key: ff318ce9b8324e455770c26ee8bf608139ffa928' cette ligne montre que la clé de signature est charger.
La quatrième ligne choisit est [    2.597520] [TTM] Zone  kernel: Available graphics memory: 507382 kiB cette ligne montre que la memoire pour le kernel est de 507382 kiB.
La cinquième ligne  choisit est [    6.352862] XFS (sda1): Ending clean mount cette ligne montre que le système de fichier est XFS et que le montage est terminé sans erreur.

Non le kernel ne continue pas de generer des logs une fois la machine allumé.
