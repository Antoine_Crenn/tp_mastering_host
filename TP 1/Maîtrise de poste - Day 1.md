# Maîtrise de poste - Day 1

## Host OS

On cherche à déterminé les informations principales de la machine, les informations que l'on cherche sont le nom de la machine, l'OS et la version de l'OS, l'architecture processeur, le modèle du processeur, la quantité de RAM et leurs modèles.

Une commande possible pour afficher le nom de la machine

```
PS C:\Users\Crenn Antoine> wmic DESKTOPMONITOR get systemName
SystemName
ANTOINECRENN
```

Le résultat de la commande montre que le nom de la machine est ANTOINECRENN.

Une commande possible pour afficher le modèle du processeur.

```
PS C:\Users\Crenn Antoine> Get-CimInstance -ClassName Win32_Processor

DeviceID Name                                     Caption                                MaxClockSpeed SocketDesignation Manufacturer
-------- ----                                     -------                                ------------- ----------------- ------------
CPU0     Intel(R) Core(TM) i7-8565U CPU @ 1.80GHz Intel64 Family 6 Model 142 Stepping 11 1992          U3E1              GenuineIntel
```

Le résultat de la commande montre que le modèle du processeur est Intel(R) Core(TM) i7-8565U CPU @ 1.80GHz

Une commande possible pour afficher l'architecture processeur, l'OS et la version de l'OS.

```
PS C:\Users\Crenn Antoine> Get-WmiObject Win32_OperatingSystem

SystemDirectory : C:\WINDOWS\system32
Organization    :
BuildNumber     : 18363
RegisteredUser  : Crenn Antoine
SerialNumber    : 00325-96488-86369-AAOEM
Version         : 10.0.18363
```

Le résultat de la commande montre que l'architecture processeur est 32 bit, l'OS est Windows et que la version de l'OS est 10.0.18363

Une commande possible pour afficher le nombre de RAM et leurs modèles.

```
PS C:\Users\Crenn Antoine> Get-CimInstance win32_physicalmemory | Format-Table Manufacturer,Banklabel,Configuredclockspeed,Devicelocator,Capacity,Serialnumber -autosiz

Manufacturer Banklabel Configuredclockspeed Devicelocator    Capacity Serialnumber
------------ --------- -------------------- -------------    -------- ------------
80AD000080AD BANK 0                    2400 DIMM A        17179869184 7204A472
```

Le résultat de la commande montre que la quantité de RAM est 17179869184 et que le modèle est DIMM.

## Devices

On cherche à trouver des informations sur les périphériques branchés à la machine, les informations que l'on cherche sont la marque et le modèle du processeur, le nombre de processeurs, le nombre de coeurs.

Une commande possible pour afficher le modèle du processeur et la marque.

```
PS C:\Users\Crenn Antoine> Get-CimInstance -ClassName Win32_Processor

DeviceID Name                                     Caption                                MaxClockSpeed SocketDesignation Manufacturer
-------- ----                                     -------                                ------------- ----------------- ------------
CPU0     Intel(R) Core(TM) i7-8565U CPU @ 1.80GHz Intel64 Family 6 Model 142 Stepping 11 1992          U3E1              GenuineIntel
```

Le résultat de la commande nous montre que la marque du proccesseur est GenuineIntel, que le modèle du proccesseur est Intel(R) Core(TM) i7-8565U CPU @ 1.80GHz et son nom de code est Lac de Whisky.

Une commande possible pour afficher le nombre de coeurs et le nombre de processeurs.

```
PS C:\Users\Crenn Antoine> WMIC CPU Get DeviceID,NumberOfCores,NumberOfLogicalProcessors
DeviceID  NumberOfCores  NumberOfLogicalProcessors
CPU0      4              8
```

Le résultat de la commande nous montre que le nombre de coeurs est de 4 et que le nombre de processeurs est de 8.

Maintenant on cherche la marque et le modèle des enceintes intégrés, du touchpad et du disque dur. 

Une commande pour afficher les informations pour les enceintes intégrés.

```
PS C:\> wmic SOUNDDEV get Manufacturer,Name
Manufacturer          Name
Intel(R) Corporation  Son Intel(R) pour écrans
Realtek               Realtek Audio
NVIDIA                NVIDIA Virtual Audio Device (Wave Extensible) (WDM)
```

Le résultat de la commande nous montre que la marque est Realtek et que le modèle est Realtek Audio.

Une commande possible pour afficher la marque et le modèle du disque dur.

```
PS C:\Users\Crenn Antoine> wmic diskdrive get Caption,DeviceID,Model,Size,Manufacturer                                  Caption                    DeviceID            Manufacturer            Model                      Size
BC501 NVMe SK hynix 512GB  \\.\PHYSICALDRIVE0  (Standard disk drives)  BC501 NVMe SK hynix 512GB  512105932800
```

Le résultat de la commande nous montre que la marque du disque dur est (Standard disk drives) et que le modèle du disque dur est BC501 NVMe SK hynix 512GB

Désolé je n'ai pas trouvé les commandes pour le touchpad.

On cherche à trouver les informations sur le disque dur, les informations que l'on cherche sont d'identifier les différentes partitions du disque dur, de déterminer le système de fichier de chaque partion et d'expliquer la fonction de chaque partition.

Une commande pour afficher les différentes partitions du disque et leurs fonctions.

```
PS C:\> get-Partition


   DiskPath : \\?\scsi#disk&ven_nvme&prod_bc501_nvme_sk_hy#4&30a84d98&0&030000#{53f56307-b6bf-11d0-94f2-00a0c91efb8b}

PartitionNumber  DriveLetter Offset                                        Size Type
---------------  ----------- ------                                        ---- ----
1                            1048576                                     650 MB System
2                            682622976                                   128 MB Reserved
3                C           816840704                                460.77 GB Basic
4                            495567503360                                990 MB Recovery
5                            496605593600                              13.28 GB Recovery
6                            510860984320                               1.15 GB Recovery
```

Le résultat de la commande nous montre qu'il y a 6 partitions sur le disque dur, la première qui contient les fichiers système, la deuxième qui est reservé par Windows, la troisième qui est la partition qui contient les fichiers de l'utilisateurs et les partitions 4 à 6 qui sont les partitions de récupération.

Une commande pour afficher le système de fichier des partitions.

```
PS C:\Users\Crenn Antoine> get-volume

DriveLetter FriendlyName FileSystemType DriveType HealthStatus OperationalStatus SizeRemaining      Size
----------- ------------ -------------- --------- ------------ ----------------- -------------      ----
            Image        NTFS           Fixed     Healthy      OK                    162.88 MB  13.28 GB
            WINRETOOLS   NTFS           Fixed     Healthy      OK                    458.87 MB    990 MB
C           OS           NTFS           Fixed     Healthy      OK                     23.61 GB 460.77 GB
            DELLSUPPORT  NTFS           Fixed     Healthy      OK                    385.06 MB   1.15 GB
```

Le résultat de la commande nous montre que le système de fichiers des partitions est NTFS.

## Network

On cherche à afficher toutes les cartes réseaux de la machine.

Une commande possible pour afficher les cartes réseaux.

```
PS C:\Users\Crenn Antoine> Get-NetAdapter | fl Name, InterfaceIndex                                                     

Name           : VMware Network Adapter VMnet1
InterfaceIndex : 29

Name           : Connexion au réseau local* 2
InterfaceIndex : 28

Name           : VirtualBox Host-Only Network #2
InterfaceIndex : 27

Name           : Connexion au réseau local
InterfaceIndex : 26

Name           : VMware Network Adapter VMnet8
InterfaceIndex : 24

Name           : VirtualBox Host-Only Network #4
InterfaceIndex : 21

Name           : VirtualBox Host-Only Network
InterfaceIndex : 19

Name           : Wi-Fi
InterfaceIndex : 12

Name           : VirtualBox Host-Only Network #3
InterfaceIndex : 6

Name           : VirtualBox Host-Only Network #5
InterfaceIndex : 5
```

Les cartes VirtualBox Host-Only Network sont les cartes virtuelles créer pour les différents réseau Host-Only VirtualBox, les cartes VMware Network Adapter sont les cartes virtuelles créer pour VMARE la carte Wifi est la carte qui me permet de me connecter au wifi.

Nous cherchons à lister tous les ports en utilisations sur la machine.

Une commande possible pour afficher les ports en utilisations.

```
PS C:\Users\Crenn Antoine> netstat                                                                                      
Connexions actives

  Proto  Adresse locale         Adresse distante       État
  TCP    127.0.0.1:27824        AntoineCrenn:50907     ESTABLISHED
  TCP    127.0.0.1:33950        AntoineCrenn:50907     ESTABLISHED
  TCP    127.0.0.1:41627        AntoineCrenn:50907     ESTABLISHED
  TCP    127.0.0.1:41638        AntoineCrenn:50907     ESTABLISHED
  TCP    127.0.0.1:47785        AntoineCrenn:50907     ESTABLISHED
  TCP    127.0.0.1:47786        AntoineCrenn:50907     ESTABLISHED
  TCP    127.0.0.1:50089        AntoineCrenn:50907     ESTABLISHED
  TCP    127.0.0.1:50123        AntoineCrenn:50124     ESTABLISHED
  TCP    127.0.0.1:50124        AntoineCrenn:50123     ESTABLISHED
  TCP    127.0.0.1:50125        AntoineCrenn:50126     ESTABLISHED
  TCP    127.0.0.1:50126        AntoineCrenn:50125     ESTABLISHED
  TCP    127.0.0.1:50128        AntoineCrenn:50129     ESTABLISHED
  TCP    127.0.0.1:50129        AntoineCrenn:50128     ESTABLISHED
  TCP    127.0.0.1:50136        AntoineCrenn:50137     ESTABLISHED
  TCP    127.0.0.1:50137        AntoineCrenn:50136     ESTABLISHED
  TCP    127.0.0.1:50907        AntoineCrenn:27824     ESTABLISHED
  TCP    127.0.0.1:50907        AntoineCrenn:33950     ESTABLISHED
  TCP    127.0.0.1:50907        AntoineCrenn:41627     ESTABLISHED
  TCP    127.0.0.1:50907        AntoineCrenn:41638     ESTABLISHED
  TCP    127.0.0.1:50907        AntoineCrenn:47785     ESTABLISHED
  TCP    127.0.0.1:50907        AntoineCrenn:47786     ESTABLISHED
  TCP    127.0.0.1:50907        AntoineCrenn:50089     ESTABLISHED
  TCP    192.168.1.94:2267      162.159.130.234:https  ESTABLISHED
  TCP    192.168.1.94:4021      162.159.128.235:https  ESTABLISHED
  TCP    192.168.1.94:4024      ec2-18-195-48-240:3478  ESTABLISHED
  TCP    192.168.1.94:4239      ec2-52-69-49-218:https  ESTABLISHED
  TCP    192.168.1.94:5427      162.159.134.233:https  ESTABLISHED
  TCP    192.168.1.94:5756      a23-57-80-121:https    CLOSE_WAIT
  TCP    192.168.1.94:5774      a95-100-243-51:https   CLOSE_WAIT
  TCP    192.168.1.94:5778      152.199.19.161:https   CLOSE_WAIT
  TCP    192.168.1.94:6813      40.67.254.36:https     ESTABLISHED
  TCP    192.168.1.94:7558      par10s38-in-f10:https  CLOSE_WAIT
  TCP    192.168.1.94:7903      par10s34-in-f10:https  CLOSE_WAIT
  TCP    192.168.1.94:7906      par10s34-in-f10:https  CLOSE_WAIT
  TCP    192.168.1.94:8389      par21s11-in-f14:https  ESTABLISHED
  TCP    192.168.1.94:9900      151.101.122.2:https    ESTABLISHED
  TCP    192.168.1.94:10076     par21s11-in-f10:https  CLOSE_WAIT
  TCP    192.168.1.94:10093     151.101.120.157:https  ESTABLISHED
  TCP    192.168.1.94:10141     104.244.42.72:https    ESTABLISHED
  TCP    192.168.1.94:10175     104.16.142.239:https   ESTABLISHED
  TCP    192.168.1.94:10182     104.16.190.66:https    ESTABLISHED
  TCP    192.168.1.94:10187     104.17.184.177:https   ESTABLISHED
  TCP    192.168.1.94:10190     151.101.122.2:https    ESTABLISHED
  TCP    192.168.1.94:10299     par10s33-in-f14:https  ESTABLISHED
  TCP    192.168.1.94:10313     ec2-34-207-20-213:9000  ESTABLISHED
  TCP    192.168.1.94:10445     52.157.234.37:https    ESTABLISHED
  TCP    192.168.1.94:10459     161.69.169.63:https    ESTABLISHED
  TCP    192.168.1.94:10460     ec2-54-152-2-188:https  ESTABLISHED
  TCP    192.168.1.94:10461     a2-16-208-179:https    ESTABLISHED
  TCP    192.168.1.94:10462     a2-16-208-179:https    ESTABLISHED
  TCP    192.168.1.94:10487     edge-star-mini-shv-01-cdg2:https  ESTABLISHED
  TCP    192.168.1.94:10488     par21s17-in-f14:https  ESTABLISHED
  TCP    192.168.1.94:10493     par21s12-in-f10:https  ESTABLISHED
  TCP    192.168.1.94:10505     ec2-52-54-178-155:https  ESTABLISHED
  TCP    192.168.1.94:10506     ec2-52-54-178-155:https  ESTABLISHED
  TCP    192.168.1.94:10529     par10s33-in-f3:https   ESTABLISHED
  TCP    192.168.1.94:10530     par21s17-in-f14:https  ESTABLISHED
  TCP    192.168.1.94:10533     par21s11-in-f3:https   ESTABLISHED
  TCP    192.168.1.94:10534     par10s34-in-f14:https  ESTABLISHED
  TCP    192.168.1.94:10535     par10s34-in-f14:https  ESTABLISHED
  TCP    192.168.1.94:10540     par10s27-in-f195:https  ESTABLISHED
  TCP    192.168.1.94:10541     13.107.6.171:https     ESTABLISHED
  TCP    192.168.1.94:40968     wo-in-f188:https       ESTABLISHED
  TCP    192.168.1.94:40972     server-13-227-198-62:https  ESTABLISHED
  TCP    192.168.1.94:40993     par21s05-in-f138:https  ESTABLISHED
  TCP    192.168.1.94:41239     ec2-52-22-229-78:https  ESTABLISHED
  TCP    192.168.1.94:41261     ec2-52-202-204-167:http  ESTABLISHED
  TCP    192.168.1.94:43693     customer:https         ESTABLISHED
  TCP    192.168.1.94:44954     13.107.18.11:https     ESTABLISHED
  TCP    192.168.1.94:49506     52.97.233.2:imaps      ESTABLISHED
  TCP    192.168.1.94:49509     40.67.254.36:https     ESTABLISHED
  TCP    [::1]:10728            AntoineCrenn:9229      SYN_SENT
  TCP    [::1]:10729            AntoineCrenn:9229      SYN_SENT
```

Les ports qui ont dans la destination https doivent aller vers internet. Mais pour les autres ports il est impossible de déterminer les différents programmes qui tourne derrière.

## Users

Nous cherchons à afficher la liste compléte des utilisateurs sur la machine.

Une commande possible pour afficher les utilisateurs sur la machine.

```
PS C:\> Get-localuser

----               ------- -----------
Administrateur     False   Compte d’utilisateur d’administration
Crenn Antoine      True
DefaultAccount     False   Compte utilisateur géré par le système.
Invité             False   Compte d’utilisateur invité
SQLEXPRESS01       True    Local user account for execution of R scripts in SQL Server instance SQLEXPRESS
SQLEXPRESS02       True    Local user account for execution of R scripts in SQL Server instance SQLEXPRESS
SQLEXPRESS03       True    Local user account for execution of R scripts in SQL Server instance SQLEXPRESS
SQLEXPRESS04       True    Local user account for execution of R scripts in SQL Server instance SQLEXPRESS
SQLEXPRESS05       True    Local user account for execution of R scripts in SQL Server instance SQLEXPRESS
SQLEXPRESS06       True    Local user account for execution of R scripts in SQL Server instance SQLEXPRESS
SQLEXPRESS07       True    Local user account for execution of R scripts in SQL Server instance SQLEXPRESS
SQLEXPRESS08       True    Local user account for execution of R scripts in SQL Server instance SQLEXPRESS
SQLEXPRESS09       True    Local user account for execution of R scripts in SQL Server instance SQLEXPRESS
SQLEXPRESS10       True    Local user account for execution of R scripts in SQL Server instance SQLEXPRESS
SQLEXPRESS11       True    Local user account for execution of R scripts in SQL Server instance SQLEXPRESS
SQLEXPRESS12       True    Local user account for execution of R scripts in SQL Server instance SQLEXPRESS
SQLEXPRESS13       True    Local user account for execution of R scripts in SQL Server instance SQLEXPRESS
SQLEXPRESS14       True    Local user account for execution of R scripts in SQL Server instance SQLEXPRESS
SQLEXPRESS15       True    Local user account for execution of R scripts in SQL Server instance SQLEXPRESS
SQLEXPRESS16       True    Local user account for execution of R scripts in SQL Server instance SQLEXPRESS
SQLEXPRESS17       True    Local user account for execution of R scripts in SQL Server instance SQLEXPRESS
SQLEXPRESS18       True    Local user account for execution of R scripts in SQL Server instance SQLEXPRESS
SQLEXPRESS19       True    Local user account for execution of R scripts in SQL Server instance SQLEXPRESS
SQLEXPRESS20       True    Local user account for execution of R scripts in SQL Server instance SQLEXPRESS
WDAGUtilityAccount False   Compte d’utilisateur géré et utilisé par le système pour les scénarios Windows Defender A...
YNOV01             True    Local user account for execution of R scripts in SQL Server instance YNOV
YNOV02             True    Local user account for execution of R scripts in SQL Server instance YNOV
YNOV03             True    Local user account for execution of R scripts in SQL Server instance YNOV
YNOV04             True    Local user account for execution of R scripts in SQL Server instance YNOV
YNOV05             True    Local user account for execution of R scripts in SQL Server instance YNOV
YNOV06             True    Local user account for execution of R scripts in SQL Server instance YNOV
YNOV07             True    Local user account for execution of R scripts in SQL Server instance YNOV
YNOV08             True    Local user account for execution of R scripts in SQL Server instance YNOV
YNOV09             True    Local user account for execution of R scripts in SQL Server instance YNOV
YNOV10             True    Local user account for execution of R scripts in SQL Server instance YNOV
YNOV11             True    Local user account for execution of R scripts in SQL Server instance YNOV
YNOV12             True    Local user account for execution of R scripts in SQL Server instance YNOV
YNOV13             True    Local user account for execution of R scripts in SQL Server instance YNOV
YNOV14             True    Local user account for execution of R scripts in SQL Server instance YNOV
YNOV15             True    Local user account for execution of R scripts in SQL Server instance YNOV
YNOV16             True    Local user account for execution of R scripts in SQL Server instance YNOV
YNOV17             True    Local user account for execution of R scripts in SQL Server instance YNOV
YNOV18             True    Local user account for execution of R scripts in SQL Server instance YNOV
YNOV19             True    Local user account for execution of R scripts in SQL Server instance YNOV
YNOV20             True    Local user account for execution of R scripts in SQL Server instance YNOV
```

Le résultat de la commande nous montre que le  nom d'utilisateur qui est full admin sur la machine est Administrateur.

## Processus

Nous cherchons à afficher les différents proccessus de la machine.

Une commande possible pour afficher les différents processus qui tourne sur la machine.

```
PS C:\> wmic PROCESS list brief
HandleCount  Name                                                                Priority  ProcessId  ThreadCount  WorkingSetSize
0            System Idle Process                                                 0         0          8            8192
9354         System                                                              8         4          225          25620480
0            Registry                                                            8         120        4            99237888
53           smss.exe                                                            11        584        2            311296
1122         csrss.exe                                                           13        992        15           2252800
156          wininit.exe                                                         13        620        1            897024
1157         csrss.exe                                                           13        636        15           3854336
1051         services.exe                                                        9         652        11           7192576
3612         lsass.exe                                                           9         660        8            18776064
86           svchost.exe                                                         8         1124       2            974848
1504         WUDFHost.exe                                                        8         1152       18           5652480
1479         svchost.exe                                                         8         1172       26           46264320
32           fontdrvhost.exe                                                     8         1180       5            761856
277          winlogon.exe                                                        13        1272       5            5726208
1846         svchost.exe                                                         8         1356       15           18149376
397          svchost.exe                                                         8         1408       5            4521984
32           fontdrvhost.exe                                                     8         1456       5            5206016
1518         dwm.exe                                                             13        1552       13           97009664
188          svchost.exe                                                         8         1620       5            2797568
281          svchost.exe                                                         8         1628       8            3633152
193          svchost.exe                                                         8         1636       4            3842048
171          svchost.exe                                                         8         1644       4            2441216
401          svchost.exe                                                         8         1736       11           4911104
262          svchost.exe                                                         8         1780       4            4907008
168          svchost.exe                                                         8         1792       5            5136384
366          svchost.exe                                                         8         1936       3            3796992
174          svchost.exe                                                         8         1960       14           1376256
290          svchost.exe                                                         8         1996       7            4435968
318          svchost.exe                                                         8         2020       12           5574656
219          svchost.exe                                                         8         1116       10           3153920
179          svchost.exe                                                         8         1348       4            2011136
189          svchost.exe                                                         8         2112       3            7102464
323          svchost.exe                                                         8         2344       11           5492736
211          svchost.exe                                                         8         2380       5            3043328
260          NVDisplay.Container.exe                                             8         2464       6            3866624
305          svchost.exe                                                         8         2496       4            14565376
225          svchost.exe                                                         8         2608       4            2351104
514          svchost.exe                                                         8         2636       9            11759616
188          svchost.exe                                                         8         2724       5            2392064
200          svchost.exe                                                         8         2768       5            3268608
248          svchost.exe                                                         8         2788       3            6074368
305          svchost.exe                                                         8         2820       3            1515520
234          svchost.exe                                                         8         2844       5            7888896
0            Memory Compression                                                  8         3000       4098         1304154112
426          svchost.exe                                                         8         3048       14           8716288
226          svchost.exe                                                         8         2180       2            4149248
221          svchost.exe                                                         8         3116       6            2486272
185          svchost.exe                                                         8         3256       4            3305472
189          svchost.exe                                                         8         3284       6            4755456
384          svchost.exe                                                         8         3292       7            5521408
1710         svchost.exe                                                         8         3360       6            5750784
3965         svchost.exe                                                         8         3540       9            7401472
356          WUDFHost.exe                                                        13        3564       13           6324224
423          svchost.exe                                                         8         3700       13           9207808
141          svchost.exe                                                         8         3868       5            2682880
577          svchost.exe                                                         8         3924       16           12619776
268          svchost.exe                                                         8         3572       5            5226496
776          spoolsv.exe                                                         8         4216       18           16408576
447          svchost.exe                                                         8         4296       13           10133504
154          armsvc.exe                                                          8         4500       2            1249280
596          svchost.exe                                                         8         4512       15           28942336
212          AdobeUpdateService.exe                                              8         4524       5            2011136
164          AdskLicensingService.exe                                            8         4532       16           4546560
418          AdAppMgrSvc.exe                                                     8         4540       10           9424896
176          AVISION_WIA_SERVICE.exe                                             8         4548       3            2035712
359          EvtEng.exe                                                          8         4556       18           5881856
115          esif_uf.exe                                                         13        4564       3            1044480
452          svchost.exe                                                         8         4572       6            13692928
233          FNPLicensingService.exe                                             8         4588       6            3297280
200          svchost.exe                                                         8         4600       3            2068480
214          svchost.exe                                                         8         4620       4            2031616
182          IntelCpHDCPSvc.exe                                                  8         4644       4            1642496
492          AGSService.exe                                                      8         4652       3            7856128
416          svchost.exe                                                         8         4660       19           36732928
700          svchost.exe                                                         8         4664       18           23154688
267          svchost.exe                                                         8         4696       6            2347008
2532         ModuleCoreService.exe                                               8         4712       68           65634304
271          svchost.exe                                                         8         4740       6            4116480
288          AGMService.exe                                                      8         4752       3            3026944
377          mfemms.exe                                                          8         4764       26           6483968
381          IntelAudioService.exe                                               8         4812       10           5693440
153          nssm.exe                                                            8         4936       1            1159168
162          PEFService.exe                                                      8         5056       2            2007040
185          RegSrvc.exe                                                         8         5068       2            3035136
135          svchost.exe                                                         8         5076       2            1093632
514          svchost.exe                                                         8         5084       6            6500352
997          sqlservr.exe                                                        8         5092       80           86388736
154          RstMwService.exe                                                    8         3836       6            1015808
300          RtkAudUService64.exe                                                8         1744       10           2920448
56           SessionService.exe                                                  8         4040       2            999424
470          dasHost.exe                                                         8         5192       6            8527872
747          ReportingServicesService.exe                                        8         5248       60           49799168
933          svchost.exe                                                         8         5292       13           9289728
146          sqlwriter.exe                                                       8         5300       2            1736704
338          vmware-authd.exe                                                    8         5332       6            4460544
94           vmnetdhcp.exe                                                       8         5376       2            1187840
218          vmware-usbarbitrator64.exe                                          8         5388       3            2424832
181          vmnat.exe                                                           8         5396       4            2420736
581          sqlceip.exe                                                         6         5404       13           21856256
997          sqlservr.exe                                                        8         5444       78           90853376
638          sqlceip.exe                                                         6         5456       12           21499904
1144         MsMpEng.exe                                                         8         5468       8            32968704
579          ZeroConfigService.exe                                               8         5480       34           8818688
740          ReportingServicesService.exe                                        8         5488       58           49385472
443          svchost.exe                                                         8         5504       7            14823424
243          WavesSysSvc64.exe                                                   8         5516       4            1748992
249          svchost.exe                                                         8         5548       6            1744896
143          IntelCpHeciSvc.exe                                                  8         5772       4            1466368
214          svchost.exe                                                         8         6124       15           2760704
442          svchost.exe                                                         8         6528       14           4784128
156          conhost.exe                                                         8         6720       4            1228800
370          svchost.exe                                                         8         6960       13           5001216
188          unsecapp.exe                                                        8         7144       4            3293184
12010690     MMSSHOST.exe                                                        8         6800       96           51216384
589          WmiPrvSE.exe                                                        8         7308       11           35229696
354          mfevtps.exe                                                         8         7324       8            9199616
317          ProtectedModuleHost.exe                                             8         7600       18           5791744
3555         WmiPrvSE.exe                                                        8         7960       16           68096000
735          Microsoft.ReportingServices.Portal.WebHost.exe                      8         8924       7            9084928
151          conhost.exe                                                         8         9044       4            1323008
129          svchost.exe                                                         8         9156       2            1286144
733          Microsoft.ReportingServices.Portal.WebHost.exe                      8         6088       7            8876032
151          conhost.exe                                                         8         8960       4            1323008
1123         MfeAVSvc.exe                                                        8         9504       63           114102272
190          mcapexe.exe                                                         8         9556       8            2383872
593          McCSPServiceHost.exe                                                8         9688       23           15937536
715          mcshield.exe                                                        8         8796       29           20467712
526          Launchpad.exe                                                       8         10952      29           4280320
524          Launchpad.exe                                                       8         10976      29           4284416
103          fdlauncher.exe                                                      8         11376      1            880640
103          fdlauncher.exe                                                      8         11388      1            872448
208          fdhost.exe                                                          8         11600      10           1028096
118          conhost.exe                                                         8         11644      2            999424
208          fdhost.exe                                                          8         11664      10           888832
118          conhost.exe                                                         8         11692      2            1019904
776          svchost.exe                                                         8         12188      16           13246464
164          svchost.exe                                                         8         1840       4            3551232
206          dllhost.exe                                                         8         12452      4            4894720
298          svchost.exe                                                         8         12732      3            5988352
273          svchost.exe                                                         8         13064      9            4399104
712          NVDisplay.Container.exe                                             8         4448       26           18169856
230          RtkAudUService64.exe                                                8         8596       3            2490368
951          sihost.exe                                                          8         13352      11           27451392
496          svchost.exe                                                         8         13404      14           21811200
233          PresentationFontCache.exe                                           8         13432      4            2072576
664          svchost.exe                                                         8         13452      8            30310400
351          taskhostw.exe                                                       8         13568      8            14295040
309          svchost.exe                                                         8         13576      8            13934592
6622         igfxEM.exe                                                          8         13796      60           16470016
414          svchost.exe                                                         8         14104      8            8998912
94           dptf_helper.exe                                                     8         14112      3            2023424
3859         explorer.exe                                                        8         14436      96           142422016
181          GoogleCrashHandler.exe                                              4         14664      5            1150976
160          GoogleCrashHandler64.exe                                            4         14720      3            1036288
303          svchost.exe                                                         8         14836      7            5361664
159          svchost.exe                                                         8         14940      2            3395584
327          svchost.exe                                                         8         15048      6            16191488
146          dllhost.exe                                                         8         15336      2            5890048
808          StartMenuExperienceHost.exe                                         8         13828      13           61648896
504          RuntimeBroker.exe                                                   8         13696      6            22003712
775          RuntimeBroker.exe                                                   8         15704      14           33554432
654          SettingSyncHost.exe                                                 6         15168      5            1585152
523          ctfmon.exe                                                          13        16440      11           8892416
305          TabTip.exe                                                          13        16468      6            6139904
616          LockApp.exe                                                         8         16636      13           32546816
407          RuntimeBroker.exe                                                   8         16788      5            20107264
161          svchost.exe                                                         8         16936      3            2850816
446          svchost.exe                                                         8         16568      7            9756672
349          svchost.exe                                                         8         16956      8            13377536
472          RuntimeBroker.exe                                                   8         17820      4            11423744
377          RuntimeBroker.exe                                                   8         18164      2            19505152
734          svchost.exe                                                         8         18224      9            21696512
300          RtkAudUService64.exe                                                8         17672      10           4554752
400          svchost.exe                                                         8         18552      4            13983744
533          WavesSvc64.exe                                                      8         18700      9            7970816
325          CastSrv.exe                                                         8         18764      7            8007680
1920         ModuleCoreService.exe                                               8         18524      28           18534400
121          conhost.exe                                                         8         17656      2            929792
232          svchost.exe                                                         8         18684      2            4804608
219          AgentAntidote.exe                                                   8         18824      1            3764224
423          AgentAntidote.exe                                                   8         19752      5            12578816
240          svchost.exe                                                         8         19860      4            5779456
413          svchost.exe                                                         8         20028      9            11857920
332          ScanToPCActivationApp.exe                                           8         20292      6            8744960
746          svchost.exe                                                         8         15860      3            5378048
677          Scanner Mouse.exe                                                   8         6956       26           19156992
202          moon.exe                                                            8         10080      1            3104768
481          svchost.exe                                                         8         4844       9            8593408
192          SoundTouchHelper.exe                                                8         5324       2            3239936
229          Scanner Mouse Monitoring.exe                                        8         4320       6            2347008
868          Creative Cloud.exe                                                  8         10520      33           26562560
107          svchost.exe                                                         8         2436       2            1028096
272          AdobeIPCBroker.exe                                                  8         19256      15           5001216
982          Adobe Desktop Service.exe                                           8         18132      35           31084544
437          Adobe CEF Helper.exe                                                8         11048      18           22175744
44           CCLibrary.exe                                                       8         21228      1            360448
786          node.exe                                                            8         21280      23           13803520
118          conhost.exe                                                         8         21380      2            1081344
325          CoreSync.exe                                                        8         20664      20           6873088
444          Adobe CEF Helper.exe                                                8         21336      18           18538496
68           CCXProcess.exe                                                      8         6096       1            544768
672          node.exe                                                            8         16912      23           34471936
121          conhost.exe                                                         8         20344      2            1077248
367          QtWebEngineProcess.exe                                              8         17176      12           13402112
412          QtWebEngineProcess.exe                                              4         18044      14           43048960
328          IAStorIcon.exe                                                      8         14048      7            5697536
1100         WinStore.App.exe                                                    8         668        17           1765376
435          ApplicationFrameHost.exe                                            8         16948      3            18948096
375          RuntimeBroker.exe                                                   8         19036      4            15364096
253          RuntimeBroker.exe                                                   8         18236      2            2195456
195          aesm_service.exe                                                    8         21968      2            1380352
218          jhi_service.exe                                                     8         22044      6            1085440
329          svchost.exe                                                         8         22116      8            5308416
266          DDVRulesProcessor.exe                                               8         22216      6            3981312
153          SocketHeciServer.exe                                                8         22316      2            1241088
21230        Dell.D3.WinSvc.exe                                                  8         22360      3833         136884224
829          DSAPI.exe                                                           8         22484      36           41897984
1081         DellSupportAssistRemedationService.exe                              8         22592      23           23580672
1625         ServiceShell.exe                                                    8         22684      36           45715456
211          msdtc.exe                                                           8         22176      9            1130496
445          IAStorDataMgrSvc.exe                                                8         22304      10           17813504
290          LMS.exe                                                             8         8324       4            2240512
89           SgrmBroker.exe                                                      8         13072      5            4886528
1011         SupportAssistAgent.exe                                              8         9132       20           48099328
390          DDVDataCollector.exe                                                8         18756      6            11763712
154          DDVCollectorSvcApi.exe                                              8         18472      2            1306624
134          nvapiw.exe                                                          8         13484      2            1380352
226          svchost.exe                                                         8         19720      2            5455872
374          AdobeNotificationClient.exe                                         8         24308      13           4038656
837          MicrosoftEdge.exe                                                   8         3012       34           1662976
358          dllhost.exe                                                         8         15952      6            7901184
162          CompPkgSrv.exe                                                      8         26720      1            4427776
1552         ShellExperienceHost.exe                                             8         26576      36           56569856
597          RuntimeBroker.exe                                                   8         24812      13           23261184
272          WUDFHost.exe                                                        8         17344      5            1306624
879          OfficeClickToRun.exe                                                8         25288      19           23511040
163          AppVShNotify.exe                                                    8         24460      1            1032192
162          AppVShNotify.exe                                                    8         28952      1            1036288
972          SearchIndexer.exe                                                   8         30420      29           32722944
465          taskhostw.exe                                                       8         12068      5            9633792
199          svchost.exe                                                         8         18784      5            2723840
231          RuntimeBroker.exe                                                   8         36984      2            7671808
146          svchost.exe                                                         8         22640      2            3104768
114          svchost.exe                                                         8         29256      3            2461696
494          SystemSettingsBroker.exe                                            8         42728      14           13975552
232          svchost.exe                                                         8         43512      6            3551232
867          audiodg.exe                                                         8         46096      37           44912640
523          WindowsInternal.ComposableShell.Experiences.TextInput.InputApp.exe  8         40664      11           26714112
122          rundll32.exe                                                        8         39536      1            1703936
995          OneDrive.exe                                                        8         53464      29           35942400
599          servicehost.exe                                                     8         39972      96           15347712
559          uihost.exe                                                          8         66456      55           8663040
183          unsecapp.exe                                                        8         12752      3            3784704
1379         SearchUI.exe                                                        8         103732     46           76394496
1306         Microsoft.Photos.exe                                                8         73816      15           22007808
443          RuntimeBroker.exe                                                   8         103440     5            19419136
126          cmd.exe                                                             8         108616     1            950272
144          conhost.exe                                                         8         57020      3            2809856
262          browserhost.exe                                                     8         78216      31           2113536
928          YourPhone.exe                                                       8         106008     15           16740352
417          RuntimeBroker.exe                                                   8         109484     4            14942208
157          openvpnserv.exe                                                     8         123640     2            4546560
223          svchost.exe                                                         8         124092     5            6549504
3327         POWERPNT.EXE                                                        8         138420     55           91254784
198          svchost.exe                                                         8         137272     7            6770688
147          svchost.exe                                                         8         137320     3            5971968
513          Antidote.exe                                                        8         135212     20           39665664
754          Video.UI.exe                                                        8         140292     16           7540736
182          RuntimeBroker.exe                                                   8         140348     2            7856128
161          igfxCUIService.exe                                                  8         131260     2            6189056
422          chrome.exe                                                          4         144060     18           106504192
194          openvpn-gui.exe                                                     8         146568     1            8843264
636          nvcontainer.exe                                                     8         126168     36           21934080
131          rundll32.exe                                                        8         135268     2            5574656
432          nvcontainer.exe                                                     8         145488     27           17596416
629          NVIDIA Web Helper.exe                                               8         143040     91           22540288
121          conhost.exe                                                         8         139460     2            4665344
504          smartscreen.exe                                                     8         149808     15           26312704
203          svchost.exe                                                         8         150552     4            5906432
534          McUICnt.exe                                                         8         142848     25           3235840
294          chrome.exe                                                          4         158944     17           38350848
205          svchost.exe                                                         8         173132     5            7106560
405          wlanext.exe                                                         8         161224     9            14262272
118          conhost.exe                                                         8         157536     2            4694016
207          svchost.exe                                                         8         161436     3            8146944
921          powershell.exe                                                      8         164716     16           93831168
264          conhost.exe                                                         8         165752     4            15761408
612          powershell.exe                                                      8         95484      11           79560704
265          conhost.exe                                                         8         97552      4            14741504
209          QcShm.exe                                                           8         81352      9            9895936
127          svchost.exe                                                         8         165880     3            8769536
235          WMIC.exe                                                            8         161404     8            13541376
```

Le service explorer permet d'avoir le bureau windows, la barre des tâches et les fenêtres de l'explorateur de fichiers. le service System correspond au service permettant de faire fonctionner le système. Les services svchosts sont des services système.

Le langage natif de Windows est Langage PowerShell.

## Scripting

Le premier script qui donne le nom de la machine, l'IP principale, l'OS et la version de l'OS, la date et l'heure d'allumage de l'ordinateur, détermine si l'OS est à jour, donne l'espace RAM utilisé et l'espace RAM disponible, donne l'espace disque utilisé et l'espace disque disponible, liste les utilisateurs de la machine et calcul le temps de réponse moyen vers 8.8.8.8

```
$nom = wmic DESKTOPMONITOR get systemName
$nom = $nom[2]
$ip = wmic NICCONFIG get IPAddress
$ip = $ip[8]
$os = wmic OS get SystemDirectory 
$os = $os[2]
$version_os = wmic OS get Version 
$version_os = $version_os[2]
$heure = wmic OS get LastBootUpTime
$heure = $heure[2]
$ajour = wmic OS get status
$ajour = $ajour[2]

function Volume {
    $os =  Get-WmiObject Win32_logicaldisk
    foreach($element in $os){
        $stockage_utilise = [math]::Round($element.Size/1000000000,2)
        $stockage_libre = [math]::Round($element.FreeSpace/1000000000,2)
        Write-Output "Le stockage utilise de la machine est $stockage_utilise Go"
        Write-Output "Le stockage libre de la machine est $stockage_libre"
    }
}

function Ram {
    $os = Get-WmiObject Win32_OperatingSystem
    $RAM_utilise = [math]::Round(($os.TotalVisibleMemorySize-$os.FreePhysicalMemory) / 1000000, 2)
    $RAM_libre = [math]::Round((Get-WmiObject win32_operatingsystem).FreePhysicalMemory / 1000000, 2)
    Write-Output "La RAM utilise de la machine est $RAM_utilise Mo"
    Write-Output "La RAM libre de la machine est $RAM_libre Mo"
}

$utilisateur = Get-localuser

function Ping_Moyenne {
    $ping = Test-Connection -count 10 8.8.8.8
    $moyenne = ($ping | Measure-Object ResponseTime -average -maximum)
    $calcul = [math]::Round($moyenne.average, 2)
    Write-Output "Le temps moyenne vers 8.8.8.8 est $calcul"
}

echo "Le nom de la machine est $nom"
echo "L'adresse IP sur la machine est $ip"
echo "L'OS de la machine est $os"
echo "La version de l'OS de la machine est $version_os"
echo "L'heure d'allumage est $heure"
echo "L'os à jour $ajour" 
$(Volume)
$(RAM)
echo "Tous les utilisateurs sont $utilisateur"
$(Ping_Moyenne)
```

Le deuxième script qui demande si l'utilisateur veux eteindre son ordinateur ou le mettre en veille et après combien de temps.

```
$valeur = Read-Host "Entrer la durée avant d'executer le script en seconde"
$eteindre = Read-Host "Si vous voulez eteindre votre ordinateur taper oui"
Start-Sleep -Seconds $valeur
if ($eteindre -eq "oui") {
    Stop-Computer
} else {
    rundll32.exe user32.dll,LockWorkStation
}
```

## Gestion de softs

L'interet de l'utilisation d'un gestionnaire de paquets est de pouvoir télécharger des applications certifiers.

Le gestionnaire de paquet propre à l'OS est chocolatey.

On cherche à lister tous les paquets installé sur la machine.

La commande pour le faire est choco search all.

```
PS C:\Users\Crenn Antoine> choco search all
Chocolatey v0.10.15
choco-upgrade-all-at 0.0.5 [Approved]
ripgrep-all 0.9.3 [Approved] Downloads cached for licensed users
allpairs 1.2.1.1 [Approved] Downloads cached for licensed users - Possibly broken for FOSS users (due to original download location changes by vendor)
allbrowsers 1.0
all-to-mp3 0.3.3 [Approved] Downloads cached for licensed users
resharper-ultimate-all 2020.1.2 [Approved]
build-them-all 0.0.8 [Approved]
selenium-all-drivers 4.0 [Approved]
pygtk-all-in-one_win32_py2.7 2.24.2.2014021601 - Possibly broken
alldup 4.4.26 [Approved]
vcredist-all 1.0.0 [Approved] - Possibly broken
explorer-show-all-folders 1.0.0 [Approved]
choco-upgrade-all-at-startup 2018.08.23 [Approved]
allwemo 6.75 [Approved]
acmesharp-posh-all 0.8.1.0 [Approved] - Possibly broken
GoogleChrome-AllUsers 26.0.1410.64
convertall 0.6.0 [Approved] Downloads cached for licensed users
windows-sdk-10-version-1803-all 10.0.17134.12 [Approved] Downloads cached for licensed users
windows-sdk-10-version-1809-all 10.0.17763.132 [Approved] Downloads cached for licensed users
windows-sdk-10-version-1903-all 10.0.18362.0 [Approved] Downloads cached for licensed users
irfanviewplugins 4.54 [Approved]
allway-sync 19.1.5 [Approved] Downloads cached for licensed users
platonnetwork-all 0.6.0 [Approved]
epidata 4.6.0.2 [Approved] Downloads cached for licensed users
windows-adk-all 10.0.18362.1 [Approved] Downloads cached for licensed users
WindowsADK.All 2.0 [Approved]
[...]
1821 packages found.

Did you know Pro / Business automatically syncs with Programs and
 Features? Learn more about Package Synchronizer at
 https://chocolatey.org/compare
```
 
 J'ai raccourci le résultat de la commande car il était trop long. On voit qu'il y a 1821 paquets.
 
 La commande pour afficher la source de choco.
 
```
PS C:\Users\Crenn Antoine> choco source
Chocolatey v0.10.15
2 validations performed. 1 success(es), 1 warning(s), and 0 error(s).

chocolatey - https://chocolatey.org/api/v2/ | Priority 0|Bypass Proxy - False|Self-Service - False|Admin Only - False.
```

Le résultat de la commande nous montre que la source des paquets est https://chocolatey.org/api/v2/

## Partage de fichiers

Pour faire le partage de fichier sur Windows on fait clique droit sur uhn dossier propriété et partager.

On cherche à trouver le processus qui est dédier au partage de fichier.

Une commande possible pour afficher les processus.

```
PS C:\Users\Crenn Antoine> wmic PROCESS list brief
HandleCount  Name                                            Priority  ProcessId  ThreadCount  WorkingSetSize
0            System Idle Process                             0         0          8            8192
10163        System                                          8         4          264          5648384
0            Registry                                        8         120        4            60379136
53           smss.exe                                        11        584        2            286720
1157         csrss.exe                                       13        908        15           2113536
156          wininit.exe                                     13        1012       1            962560
1396         csrss.exe                                       13        96         18           4390912
1092         services.exe                                    9         608        14           8228864
3085         lsass.exe                                       9         652        10           18903040
86           svchost.exe                                     8         1056       2            1146880
32           fontdrvhost.exe                                 8         1080       5            1024000
1552         svchost.exe                                     8         1088       28           56041472
1538         WUDFHost.exe                                    8         1136       19           5537792
1958         svchost.exe                                     8         1216       16           20471808
408          svchost.exe                                     8         1264       7            4554752
277          winlogon.exe                                    13        1324       5            4329472
32           fontdrvhost.exe                                 8         1400       5            4087808
1597         dwm.exe                                         13        1476       14           111992832
281          svchost.exe                                     8         1552       8            3600384
187          svchost.exe                                     8         1560       5            1769472
176          svchost.exe                                     8         1568       4            3313664
200          svchost.exe                                     8         1576       4            3846144
398          svchost.exe                                     8         1700       6            4263936
248          svchost.exe                                     8         1768       3            5689344
262          svchost.exe                                     8         1776       4            4050944
436          svchost.exe                                     8         1788       20           10428416
262          svchost.exe                                     8         1912       3            3551232
2736         svchost.exe                                     8         1920       6            5758976
174          svchost.exe                                     8         1944       14           1626112
201          svchost.exe                                     8         2000       5            4907008
166          svchost.exe                                     8         2008       3            5050368
326          svchost.exe                                     8         1076       14           5353472
217          svchost.exe                                     8         1692       10           3280896
179          svchost.exe                                     8         2080       4            2203648
227          svchost.exe                                     8         2320       2            4698112
292          svchost.exe                                     8         2340       12           4292608
211          svchost.exe                                     8         2484       5            3325952
176          igfxCUIService.exe                              8         2544       2            1929216
310          NVDisplay.Container.exe                         8         2620       7            4214784
342          svchost.exe                                     8         2644       9            16490496
359          svchost.exe                                     8         2760       18           5955584
188          svchost.exe                                     8         2768       5            2318336
505          svchost.exe                                     8         2848       8            11669504
248          svchost.exe                                     8         2988       6            8151040
357          svchost.exe                                     8         2996       3            1695744
221          svchost.exe                                     8         3004       6            2301952
0            Memory Compression                              8         2328       2682         971157504
208          svchost.exe                                     8         2308       3            3940352
191          svchost.exe                                     8         2460       7            6692864
358          WUDFHost.exe                                    13        3096       13           5922816
398          svchost.exe                                     8         3124       13           8802304
5353         svchost.exe                                     8         3448       12           6737920
530          svchost.exe                                     8         3584       18           9691136
146          svchost.exe                                     8         3800       6            2617344
575          svchost.exe                                     8         3856       14           11001856
273          svchost.exe                                     8         3956       5            4497408
399          wlanext.exe                                     8         4040       9            4083712
118          conhost.exe                                     8         4048       2            1003520
733          spoolsv.exe                                     8         2936       10           12779520
452          svchost.exe                                     8         4064       16           12537856
263          svchost.exe                                     8         4244       5            2125824
164          svchost.exe                                     8         4252       4            2781184
418          AdAppMgrSvc.exe                                 8         4364       10           13602816
172          AdskLicensingService.exe                        8         4372       16           4636672
212          AdobeUpdateService.exe                          8         4380       5            2023424
611          AGSService.exe                                  8         4392       5            10522624
172          AVISION_WIA_SERVICE.exe                         8         4404       3            1904640
324          AGMService.exe                                  8         4412       3            3604480
807          OfficeClickToRun.exe                            8         4468       19           21553152
183          svchost.exe                                     8         4476       3            2330624
143          IntelCpHeciSvc.exe                              8         4492       4            1789952
182          IntelCpHDCPSvc.exe                              8         4508       4            1892352
383          svchost.exe                                     8         4524       7            11870208
258          svchost.exe                                     8         4584       6            2965504
408          svchost.exe                                     8         4648       19           39804928
115          esif_uf.exe                                     13        4656       3            1105920
596          svchost.exe                                     8         4676       11           35618816
362          EvtEng.exe                                      8         4748       19           3887104
233          FNPLicensingService.exe                         8         4768       6            3035136
382          IntelAudioService.exe                           8         4820       8            8241152
673          svchost.exe                                     8         4836       22           23523328
274          svchost.exe                                     8         4892       6            3862528
759          servicehost.exe                                 8         4952       99           19542016
153          nssm.exe                                        8         5088       1            1003520
157          openvpnserv.exe                                 8         4216       2            1142784
135          svchost.exe                                     8         4844       2            1122304
628          nvcontainer.exe                                 8         5124       36           10129408
185          RegSrvc.exe                                     8         5168       2            1495040
154          RstMwService.exe                                8         5200       6            987136
309          RtkAudUService64.exe                            8         5228       9            3473408
292          SmartByteNetworkService.exe                     8         5252       12           33210368
470          dasHost.exe                                     8         5272       5            7593984
56           SessionService.exe                              8         5300       2            974848
94           vmnetdhcp.exe                                   8         5336       2            1216512
181          vmnat.exe                                       8         5344       4            2449408
146          sqlwriter.exe                                   8         5356       2            1699840
1851         svchost.exe                                     8         5364       6            8462336
247          WavesSysSvc64.exe                               8         5372       2            2260992
518          svchost.exe                                     8         5392       6            6348800
338          vmware-authd.exe                                8         5400       6            4419584
218          vmware-usbarbitrator64.exe                      8         5408       3            2359296
432          svchost.exe                                     8         5432       8            13275136
576          ZeroConfigService.exe                           8         5524       34           6025216
136          svchost.exe                                     8         5716       5            1617920
214          svchost.exe                                     8         5832       14           2670592
991          sqlservr.exe                                    8         5928       74           117047296
963          sqlservr.exe                                    8         5940       74           129150976
178          myCANAL.Service.exe                             8         6016       5            995328
156          conhost.exe                                     8         6108       4            966656
782          ReportingServicesService.exe                    8         6140       58           43859968
584          sqlceip.exe                                     6         5192       12           19976192
435          svchost.exe                                     8         6400       13           4374528
367          svchost.exe                                     8         6676       13           4800512
764          ReportingServicesService.exe                    8         6688       58           45850624
559          sqlceip.exe                                     6         6724       11           27017216
129          svchost.exe                                     8         8260       2            1302528
302          svchost.exe                                     8         8380       6            5861376
159          svchost.exe                                     8         8580       2            3432448
675          Microsoft.ReportingServices.Portal.WebHost.exe  8         8624       7            6242304
131          rundll32.exe                                    8         8672       2            1466368
190          unsecapp.exe                                    8         9036       4            3215360
151          conhost.exe                                     8         9044       4            884736
673          Microsoft.ReportingServices.Portal.WebHost.exe  8         9184       7            7028736
151          conhost.exe                                     8         8356       4            872448
692          NVDisplay.Container.exe                         8         9396       25           14569472
694          svchost.exe                                     8         11532      16           12230656
524          Launchpad.exe                                   8         11796      29           4182016
103          fdlauncher.exe                                  8         11864      1            1118208
525          Launchpad.exe                                   8         11876      29           4243456
103          fdlauncher.exe                                  8         11896      1            1101824
208          fdhost.exe                                      8         12116      10           1126400
118          conhost.exe                                     8         12124      2            978944
208          fdhost.exe                                      8         12160      10           1101824
118          conhost.exe                                     8         12176      2            987136
298          svchost.exe                                     8         12596      3            4972544
205          dllhost.exe                                     8         12952      5            5050368
278          svchost.exe                                     8         12812      9            4034560
195          aesm_service.exe                                8         13628      2            1388544
218          jhi_service.exe                                 8         1036       6            1011712
449          svchost.exe                                     8         8480       9            9551872
333          svchost.exe                                     8         13784      8            4702208
621          svchost.exe                                     8         13580      13           11218944
[...]
```

J'ai du raccourcit le résultat de la commande car il était trop long et problème avec Windows c'est que les processus système sont tous marqué comme svchost.exe donc on ne peut pas identifier explicitement celui qui gerer le partage de fichier.

Maintenant on cherche à prouver qu'un port permet d'acceder au partage de ficher.

Une commande possible pour afficher les ports en utilisation sur la machine.

```
PS C:\Users\Crenn Antoine> netstat

Connexions actives

  Proto  Adresse locale         Adresse distante       État
  TCP    127.0.0.1:4293         AntoineCrenn:64643     ESTABLISHED
  TCP    127.0.0.1:15608        AntoineCrenn:64643     ESTABLISHED
  TCP    127.0.0.1:15667        AntoineCrenn:64643     ESTABLISHED
  TCP    127.0.0.1:15742        AntoineCrenn:64643     ESTABLISHED
  TCP    127.0.0.1:18647        AntoineCrenn:64643     ESTABLISHED
  TCP    127.0.0.1:29653        AntoineCrenn:64643     ESTABLISHED
  TCP    127.0.0.1:30246        AntoineCrenn:64643     ESTABLISHED
  TCP    127.0.0.1:42656        AntoineCrenn:64643     ESTABLISHED
  TCP    127.0.0.1:42657        AntoineCrenn:64643     ESTABLISHED
  TCP    127.0.0.1:51713        AntoineCrenn:64643     ESTABLISHED
  TCP    127.0.0.1:57938        AntoineCrenn:64643     ESTABLISHED
  TCP    127.0.0.1:58431        AntoineCrenn:64643     ESTABLISHED
  TCP    127.0.0.1:61511        AntoineCrenn:64643     ESTABLISHED
  TCP    127.0.0.1:64643        AntoineCrenn:4293      ESTABLISHED
  TCP    127.0.0.1:64643        AntoineCrenn:15608     ESTABLISHED
  TCP    127.0.0.1:64643        AntoineCrenn:15667     ESTABLISHED
  TCP    127.0.0.1:64643        AntoineCrenn:15742     ESTABLISHED
  TCP    127.0.0.1:64643        AntoineCrenn:18647     ESTABLISHED
  TCP    127.0.0.1:64643        AntoineCrenn:29653     ESTABLISHED
  TCP    127.0.0.1:64643        AntoineCrenn:30246     ESTABLISHED
  TCP    127.0.0.1:64643        AntoineCrenn:42656     ESTABLISHED
  TCP    127.0.0.1:64643        AntoineCrenn:42657     ESTABLISHED
  TCP    127.0.0.1:64643        AntoineCrenn:51713     ESTABLISHED
  TCP    127.0.0.1:64643        AntoineCrenn:57938     ESTABLISHED
  TCP    127.0.0.1:64643        AntoineCrenn:58431     ESTABLISHED
  TCP    127.0.0.1:64643        AntoineCrenn:61511     ESTABLISHED
  TCP    192.168.1.94:1640      52.236.190.14:https    ESTABLISHED
  TCP    192.168.1.94:1824      ec2-52-198-151-126:https  ESTABLISHED
  TCP    192.168.1.94:2148      par21s17-in-f14:https  ESTABLISHED
  TCP    192.168.1.94:2149      par21s17-in-f14:https  ESTABLISHED
  TCP    192.168.1.94:2335      par21s05-in-f2:https   ESTABLISHED
  TCP    192.168.1.94:2361      par10s34-in-f14:https  ESTABLISHED
  TCP    192.168.1.94:2441      53:https               ESTABLISHED
  TCP    192.168.1.94:2479      162.159.130.233:https  TIME_WAIT
  TCP    192.168.1.94:2527      ec2-3-213-18-157:https  ESTABLISHED
  TCP    192.168.1.94:2528      ec2-3-213-18-157:https  ESTABLISHED
  TCP    192.168.1.94:2543      par21s04-in-f10:https  ESTABLISHED
  TCP    192.168.1.94:2544      par21s04-in-f10:https  ESTABLISHED
  TCP    192.168.1.94:2569      52.114.75.83:https     ESTABLISHED
  TCP    192.168.1.94:2574      ec2-54-145-139-136:9000  ESTABLISHED
  TCP    192.168.1.94:18485     par10s27-in-f202:https  ESTABLISHED
  TCP    192.168.1.94:18518     52.114.75.169:https    ESTABLISHED
  TCP    192.168.1.94:18624     ec2-35-170-0-145:https  ESTABLISHED
  TCP    192.168.1.94:18690     wa-in-f188:https       ESTABLISHED
  TCP    192.168.1.94:18770     40.67.254.36:https     ESTABLISHED
  TCP    192.168.1.94:18782     server-13-225-31-76:https  ESTABLISHED
  TCP    192.168.1.94:20910     52.114.76.163:https    ESTABLISHED
  TCP    192.168.1.94:28330     ec2-34-204-145-18:http  ESTABLISHED
  TCP    192.168.1.94:33972     ec2-3-220-126-52:https  ESTABLISHED
  TCP    192.168.1.94:49492     40.67.254.36:https     ESTABLISHED
  TCP    192.168.1.94:49495     40.101.92.18:imaps     CLOSE_WAIT
  TCP    192.168.1.94:50712     ec2-18-179-179-211:https  ESTABLISHED
  TCP    192.168.1.94:54200     a23-57-80-10:https     CLOSE_WAIT
  TCP    192.168.1.94:56220     par21s03-in-f10:https  CLOSE_WAIT
  TCP    192.168.1.94:56223     par21s17-in-f13:https  CLOSE_WAIT
  TCP    192.168.1.94:60707     par21s05-in-f138:https  CLOSE_WAIT
  TCP    192.168.1.94:61518     ec2-34-240-200-56:https  ESTABLISHED
  TCP    192.168.1.94:61888     162.159.136.234:https  TIME_WAIT
  TCP    192.168.1.94:64198     wr-in-f189:https       ESTABLISHED
  TCP    192.168.1.94:64588     174:4070               ESTABLISHED
  TCP    192.168.1.94:64590     47:https               ESTABLISHED
  TCP    192.168.1.94:64593     162.159.136.234:https  ESTABLISHED
  TCP    192.168.1.94:64673     ham02s14-in-f195:https  ESTABLISHED
```

Le port qui est utilisé par le partage de fichier est là mais il est très difficile d'identifier celui qui gerer le partage de fichier.

## Chiffrement et notion de confiance

L'information principale d'un certificat est la clé publique fournit par le site.

Les autres informations importantes contenu dans un certificat sont l'url du site, le nom de l'entreprise de certification, la signature de l'entreprise de certification et la date d'expiration du certificat.

## Chiffrement de mails

Désole je n'ai pas réussi à faire le chiffrement de mail car je n'ai pas compris comment le mettre en place.

## TLS

Le HTTPS garantit la sécurité des données car elles ne sont pas en clair et l'integrité des données car elles sont signiés.

Le cadena vert nous indique que les informations transitent de manière sécurisé et chiffrer.

Un certificat à beaucoup d'information qui sont les suivantes. Il y a la version du certificat qui permet d'afficher la version actuelle du certificat, le numéro de série qui permet d'identifier un certificat, l'algorithme de signature qui est l'algorithme qui permet de certifier à l'utilisateur que l'information qu'il recoit viens bien du site qu'il est entrain de consulter, l'algorithme de hachage est pour chiffrer les données envoyer par le site, l'émeutteur est l'autorité de certification qui a accordé ce certificat, valide à partir du est la date de début du certificat, valide jusqu'au est la date de d'expiration du certificat, l'objet est l'entité qui reçoit le certificat, la clé publique est la clé qui nous ai fournit par le site pour communiquer avec le site de manière chiffrer, autre nom de l'objet où il y a les autres noms du site, l'utilisation de la clé qui permet d'expliquer que la clé va servir pour la signature numérique et le chiffrement et enfin il y a l'empreinte numérique est une donnée permet d'identifier le certificat.

Ce qui permet de faire confiance au HTTPS de ce site sont que la clé de signature est sha256RSA, que la clé de hachage est sha256, que l'emetteur du certificat est Gandi Standard SSL CA 2, que le certificat est encore valide car il expirer en 2021, que l'objet correspond à l'url du site et que l'on a la clé publique qui est sur 2048 bits.

## SSH

### Client

Pour pouvoir se connecter avec un échange de clé il faut créer deux clé qui sont une clé publique et une clé privé qui sont créer grâce à la commande ssh-keygen, la clé qui est déposé est la clé publique, on ne depose pas l'autre clé car c'est la clé qui permet de déchiffrer les données reçu.
Le fait de déposer la clé sur le serveur distant permet que l'on est plus besoin de se connecter par mot de passe car le serveur ssh reconnait la clé privée qui est associé à la clé publique déposer. Le serveur distant pourra chiffrer les données grâce à la clé publique et ces données seront déchiffrer grâce à la clé privée.
La clé que l'on a déposer est stocké dans le fichier .ssh/authorized_keys et les permissions sur le fichier sont 644.

Le fingerprint SSH focntionne avec des fonctions de hachage et avec les fichiers il vérifie si le fichier qui contient l'empreinte de la clé publique est toujours le même que lors de la dernière connexion.

On cherche à créer un fichier /.ssh/config pour pouvoir se connecter à la VM avec le nom toto

```
PS C:\Users\Crenn Antoine> ssh -i .ssh/tp_poste toto
Last login: Fri May 15 10:58:00 2020 from 10.2.1.1
[antoine@patron ~]$
```

Le résultat de la commande nous montre que la configuration du fichier à bien marcher car on a réussit à se connecter à la VM.

## SSH avancé

### SSH tunnels

On cherche à mettre en place le ssh tunnel pour ce faire on se connecte à la CM en SSH, sur le navigateur on configurer le prxy local sur le port 7777 sur Chrome il faut aller dans les paramètres puis dans paramètres avancé puis cliquer sur ouvrir les paramètres de proxy puis cliquer sur paramètres réseau et enfin remplir la case Adresse et la case Port.

Sur la VM on installe un serveur web.

Puis en tape http://localhost sur le navigateur on obtient.

![](https://i.imgur.com/FtUHpPB.png)

### SSH jumps

Le concept du ssh jumps est de pouvoir utilisé une machine intermédiaire pour pouvoir se connecter à une machine qui n'est pas connue de notre machine mais qui est connu de la machine intermédiaire. L'utilité du ssh jumps est de pourvoir isolé les machines au maximum mais grâce à la machine intermédiaire la connexion est toujours possible par exemple pour un serveur.

J'ai pas réussit à le mettre en place car j'obteniais une erreur de ssh qui d'après internet provient du powershell.
